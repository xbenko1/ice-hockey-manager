INSERT INTO match (status, team1_score, team2_score, match_time)
VALUES
    ('PLANNED', 0, 0, '2025-06-06 16:20:00'),
    ('TEAM1_WIN', 2, 0, '2023-08-09 14:00:00'),
    ('TIE', 1, 1, '2024-10-15 18:30:00'),
    ('ONGOING', 0, 0, '2024-12-20 20:45:00'),
    ('TEAM2_WIN', 0, 3, '2023-11-05 15:00:00');