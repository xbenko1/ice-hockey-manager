package cz.fi.muni.pa165.matchService.service;

import cz.fi.muni.pa165.dtos.BaseMatchDTO;
import cz.fi.muni.pa165.exceptions.MatchNotFoundException;
import cz.fi.muni.pa165.matchService.repository.Match;


import java.util.List;

public interface MatchService {
    List<Match> getAllMatches();
    List<Match> getPlayedMatches();
    List<Match> getScheduledMatches();
    Match scheduleMatch(Match match);
    Match updateMatch(Long id, Match match) throws IllegalArgumentException, MatchNotFoundException;
    Match getMatch(Long id) throws MatchNotFoundException;
    Match deleteMatch(Long id) throws IllegalArgumentException, MatchNotFoundException;
}
