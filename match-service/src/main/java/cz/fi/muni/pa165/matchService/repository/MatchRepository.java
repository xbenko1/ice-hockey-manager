package cz.fi.muni.pa165.matchService.repository;

import cz.fi.muni.pa165.enums.MatchStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface MatchRepository extends JpaRepository<Match, Long> {
    @Query("SELECT m FROM Match m WHERE m.status IN :statuses")
    List<Match> findAllPlayed(List<MatchStatus> statuses);
    @Query("SELECT m FROM Match m WHERE m.status = :status")
    List<Match> findAllScheduled(MatchStatus status);
}
