package cz.fi.muni.pa165.matchService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class MatchServiceApp {
    private static final Logger logger = LoggerFactory.getLogger(MatchServiceApp.class);

    // Retrieve server port and context-path from application.yml
    @Value("8083")
    private String serverPort;

    @Value("/openapi")
    private String apiDocsPath;

    @Value("/swagger-ui.html")
    private String swaggerUiPath;

    public static void main(String[] args) {
        ApplicationContext ctx = SpringApplication.run(MatchServiceApp.class, args);
    }

    @Bean
    CommandLineRunner run() {
        return args -> {
            String baseUrl = "http://localhost:" + serverPort;
            logger.info("Match Service Application has started successfully.");
            logger.info("API Documentation: {}", baseUrl + apiDocsPath);
            logger.info("Swagger UI: {}", baseUrl + swaggerUiPath);
        };
    }

}
