package cz.fi.muni.pa165.matchService.service;

import cz.fi.muni.pa165.exceptions.MatchNotFoundException;
import cz.fi.muni.pa165.enums.MatchStatus;
import cz.fi.muni.pa165.matchService.repository.Match;
import cz.fi.muni.pa165.matchService.repository.MatchRepository;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.server.resource.authentication.BearerTokenAuthentication;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.util.Arrays;
import java.util.List;

@Service
public class MatchServiceImpl implements MatchService {

    static public String ID_NOT_NULL_ERROR = "id can't be null";

    private final MatchRepository matchRepository;
    private final WebClient webClient;

    @Autowired
    public MatchServiceImpl(MatchRepository matchRepository, WebClient webClient) {
        this.matchRepository = matchRepository;
        this.webClient = webClient;
    }

    @Transactional
    @Override
    public List<Match> getAllMatches() {
        return matchRepository.findAll();
    }

    @Transactional
    @Override
    public List<Match> getPlayedMatches() {
        List<MatchStatus> validStatuses = Arrays.asList(MatchStatus.TIE, MatchStatus.TEAM1_WIN, MatchStatus.TEAM2_WIN);
        return matchRepository.findAllPlayed(validStatuses);
    }

    @Transactional
    @Override
    public List<Match> getScheduledMatches() {
        return matchRepository.findAllScheduled(MatchStatus.PLANNED);
    }

    @Transactional
    @Override
    public Match scheduleMatch(Match match) {
        if (!amIAdmin()) {
            throw new IllegalArgumentException("Only admin can create a match");
        }
        if (match == null) {
            throw new IllegalArgumentException("Input DTO must not be null");
        }
        return matchRepository.save(match);
    }

    @Transactional
    @Override
    public Match updateMatch(Long id, Match match) throws IllegalArgumentException, MatchNotFoundException {
        if (!amIAdmin()) {
            throw new IllegalArgumentException("Only admin can update a match");
        }
        if (id == null) {
            throw new IllegalArgumentException(ID_NOT_NULL_ERROR);
        }
        if (match == null) {
            throw new IllegalArgumentException("Match can't be null");
        }
        if (!matchRepository.existsById(id)) {
            throw new MatchNotFoundException();
        }
        match.setId(id);
        return matchRepository.save(match);
    }

    @Transactional
    @Override
    public Match getMatch(Long id) throws IllegalArgumentException, MatchNotFoundException {
        if (id == null) {
            throw new IllegalArgumentException(ID_NOT_NULL_ERROR);
        }
        return matchRepository.findById(id).orElseThrow(MatchNotFoundException::new);
    }

    @Transactional
    @Override
    public Match deleteMatch(Long id) throws IllegalArgumentException, MatchNotFoundException {
        if (!amIAdmin()) {
            throw new IllegalArgumentException("Only admin can delete a match");
        }
        if (id == null) {
            throw new IllegalArgumentException(ID_NOT_NULL_ERROR);
        }
        Match matchToDelete = getMatch(id);
        matchRepository.delete(matchToDelete);
        return matchToDelete;
    }

    public boolean amIAdmin() {
        String jwt = extractJwtToken();
        Mono<Boolean> isAdmin = webClient.get()
                .uri("api/users/amIAdmin")
                .header("Authorization", "Bearer " + jwt)
                .retrieve()
                .bodyToMono(Boolean.class);
        return Boolean.TRUE.equals(isAdmin.block());
    }

    private String extractJwtToken() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        try {
            if (authentication instanceof BearerTokenAuthentication) {
                BearerTokenAuthentication jwtToken = (BearerTokenAuthentication) authentication;
                return jwtToken.getToken().getTokenValue();
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return "";
    }
}

