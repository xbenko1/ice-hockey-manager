package cz.fi.muni.pa165.matchService.rest;

import cz.fi.muni.pa165.dtos.MatchDTO;
import cz.fi.muni.pa165.dtos.BaseMatchDTO;
import cz.fi.muni.pa165.exceptions.MatchNotFoundException;

import cz.fi.muni.pa165.matchService.facade.MatchFacade;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.servers.Server;
import io.swagger.v3.oas.annotations.servers.ServerVariable;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springdoc.api.ErrorMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import jakarta.validation.Valid;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;


@OpenAPIDefinition(
        info = @Info(title = "Match API",
                version = "1.0",
                description = """
                    API for managing matches in the ice hockey manager system
                    API defines following operations:
                    - getting all matches
                    - schedule a match
                    - update a match
                    - get a match by id
                    - get all scheduled matches
                    - get all played matches
                    - delete a match
                    """
        ),
        servers = @Server(description = "my server", url = "{scheme}://{server}:{port}", variables = {
                @ServerVariable(name = "scheme", allowableValues = {"http", "https"}, defaultValue = "http"),
                @ServerVariable(name = "server", defaultValue = "localhost"),
                @ServerVariable(name = "port", defaultValue = "8083"),
        }))
@Tag(name = "Match", description = "tag.match.description")
@RequestMapping(path = "/api", produces = MediaType.APPLICATION_JSON_VALUE)
@RestController
public class MatchController {

    private static final Logger logger = LoggerFactory.getLogger(MatchController.class);

    private final MatchFacade matchFacade;
    @Autowired
    public MatchController(MatchFacade matchFacade) {
        this.matchFacade = matchFacade;
    }

    @Operation(summary = "operation.getAllMatches.summary")
    @GetMapping("/matches")
    public ResponseEntity<List<MatchDTO>> getAllMatches() {
        logger.info("Fetching all matches");
        return ResponseEntity.ok(matchFacade.getAllMatches());
    }

    @Operation(summary = "operation.scheduleMatch.summary",
            responses = @ApiResponse(responseCode = "201", description = "operation.scheduleMatch.description",
                    content = @Content(schema = @Schema(implementation = MatchDTO.class))
            )
    )
    @PostMapping("/matches")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<?> scheduleMatch(@Valid @RequestBody BaseMatchDTO baseMatchDTO) {
        logger.info("Scheduling a match");
        try {
            MatchDTO newMatch = matchFacade.scheduleMatch(baseMatchDTO);
            URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
                    .buildAndExpand(newMatch.getId()).toUri();
            return ResponseEntity.created(location).body(newMatch);
        } catch (IllegalArgumentException e){
            logger.error("Error creating match: {}", e.getMessage());
            return ResponseEntity.badRequest().body(new ErrorMessage(e.getMessage()));
        }
    }

    @Operation(summary = "operation.updateMatch.summary")
    @PutMapping("/match/{id}")
    public ResponseEntity<?> updateMatch(@PathVariable Long id, @Valid @RequestBody BaseMatchDTO baseMatchDTO) {
        try {
            logger.info("Updating match with id={}", id);
            return ResponseEntity.ok(matchFacade.updateMatch(id, baseMatchDTO));
        } catch (IllegalArgumentException e) {
            logger.error("Error updating match with id={}: {}", id, e.getMessage());
            return ResponseEntity.badRequest().build();
        } catch (MatchNotFoundException e) {
            logger.warn("Match not found with id={}", id);
            return ResponseEntity.notFound().build();
        }
    }

    @Operation(summary = "operation.getPlayedMatches.summary")
    @GetMapping("/matches/played")
    public ResponseEntity<List<MatchDTO>> getPlayedMatches() {
        try {
            logger.info("Fetching all played matches");
            return ResponseEntity.ok(matchFacade.getPlayedMatches());
        } catch (Exception e) {
            logger.error("Error occurred while fetching played matches: {}", e.getMessage());
            return ResponseEntity.notFound().build();
        }
    }

    @Operation(summary = "operation.getScheduledMatches.summary")
    @GetMapping("/matches/scheduled")
    public ResponseEntity<List<MatchDTO>> getScheduledMatches() {
        try {
            logger.info("Fetching all scheduled matches");
            return ResponseEntity.ok(matchFacade.getScheduledMatches());
        } catch (Exception e) {
            logger.error("Error occurred while fetching scheduled matches: {}", e.getMessage());
            return ResponseEntity.notFound().build();
        }
    }

    @Operation(summary = "operation.getMatch.summary")
    @GetMapping("/matches/{id}")
    public ResponseEntity<MatchDTO> getMatch(@PathVariable Long id) {
        try {
            logger.info("Fetching match with id={}", id);
            return ResponseEntity.ok(matchFacade.getMatch(id));
        } catch (IllegalArgumentException | MatchNotFoundException e) {
            logger.error("Error fetching match with id={}: {}", id, e.getMessage());
            return ResponseEntity.notFound().build();
        }
    }

    @Operation(summary = "operation.deleteMatch.summary")
    @DeleteMapping("/match/{id}")
    public ResponseEntity<?> deleteMatch(@PathVariable Long id) {
        try {
            logger.info("Deleting match with id={}", id);
            return ResponseEntity.ok(matchFacade.deleteMatch(id));
        } catch (IllegalArgumentException | MatchNotFoundException e) {
            logger.error("Error deleting match with id={}: {}", id, e.getMessage());
            return ResponseEntity.notFound().build();
        }
    }
}
