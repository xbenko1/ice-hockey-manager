package cz.fi.muni.pa165.matchService.repository;


import cz.fi.muni.pa165.enums.MatchStatus;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.PositiveOrZero;

import java.time.LocalDateTime;


@Schema(description = "Represents a match in the system.")
@Entity
@Table(name = "match")
public class Match {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Schema(description = "The ID of the match.", example = "1")
    private Long id;

    @Schema(description = "The status of the match.", example = "ONGOING")
    @NotNull(message = "Match status must not be null")
    @Enumerated(EnumType.STRING)
    private MatchStatus status;

    @Schema(description = "The score of the first team.", example = "3")
    @PositiveOrZero(message = "Team 1 score must be non-negative")
    @Column(name = "team1_score")
    private Integer team1Score;

    @Schema(description = "The score of the second team.", example = "2")
    @PositiveOrZero(message = "Team 2 score must be non-negative")
    @Column(name = "team2_score")
    private Integer team2Score;

    @Schema(description = "The date and time of the match.", example = "2024-04-01T18:00:00")
    @NotNull(message = "Match time must not be null")
    @Column(name = "match_time")
    private LocalDateTime matchTime;

    public Match() {}

    public Match(Long id, MatchStatus status, Integer team1Score, Integer team2Score, LocalDateTime matchTime) {
        this.id = id;
        this.status = status;
        this.team1Score = team1Score;
        this.team2Score = team2Score;
        this.matchTime = matchTime;
    }

    // Explicit getters and setters
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public MatchStatus getStatus() {
        return status;
    }

    public void setStatus(MatchStatus status) {
        this.status = status;
    }

    public Integer getTeam1Score() {
        return team1Score;
    }

    public void setTeam1Score(Integer team1Score) {
        this.team1Score = team1Score;
    }

    public Integer getTeam2Score() {
        return team2Score;
    }

    public void setTeam2Score(Integer team2Score) {
        this.team2Score = team2Score;
    }

    public LocalDateTime getMatchTime() {
        return matchTime;
    }

    public void setMatchTime(LocalDateTime matchTime) {
        this.matchTime = matchTime;
    }
}
