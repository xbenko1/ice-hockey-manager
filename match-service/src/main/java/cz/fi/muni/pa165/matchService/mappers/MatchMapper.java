package cz.fi.muni.pa165.matchService.mappers;

import cz.fi.muni.pa165.dtos.BaseMatchDTO;
import cz.fi.muni.pa165.dtos.MatchDTO;
import cz.fi.muni.pa165.matchService.repository.Match;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface MatchMapper {

    MatchDTO toDTO(Match match);

    Match toEntity(MatchDTO dto);

    Match toEntity(BaseMatchDTO baseDTO);
}
