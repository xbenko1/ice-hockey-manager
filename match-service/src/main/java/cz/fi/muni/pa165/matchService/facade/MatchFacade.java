package cz.fi.muni.pa165.matchService.facade;

import cz.fi.muni.pa165.dtos.BaseMatchDTO;
import cz.fi.muni.pa165.dtos.MatchDTO;
import cz.fi.muni.pa165.exceptions.MatchNotFoundException;
import cz.fi.muni.pa165.matchService.mappers.MatchMapper;
import cz.fi.muni.pa165.matchService.repository.Match;
import cz.fi.muni.pa165.matchService.service.MatchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class MatchFacade {

    private final MatchService matchService;
    private final MatchMapper matchMapper;

    @Autowired
    public MatchFacade(MatchService matchService, MatchMapper matchMapper) {
        this.matchService = matchService;
        this.matchMapper = matchMapper;
    }

    public List<MatchDTO> getAllMatches(){
        return matchService.getAllMatches().stream().map(matchMapper::toDTO).collect(Collectors.toList());
    }

    public List<MatchDTO> getPlayedMatches() {
        return matchService.getPlayedMatches().stream()
                .map(matchMapper::toDTO)
                .collect(Collectors.toList());
    }

    public List<MatchDTO> getScheduledMatches() {
        return matchService.getScheduledMatches().stream()
                .map(matchMapper::toDTO)
                .collect(Collectors.toList());
    }

    public MatchDTO scheduleMatch(BaseMatchDTO baseMatchDTO) {
        Match scheduledMatch = matchService.scheduleMatch(matchMapper.toEntity(baseMatchDTO));
        return matchMapper.toDTO(scheduledMatch);
    }

    public MatchDTO updateMatch(Long id, BaseMatchDTO baseMatchDTO) throws IllegalArgumentException, MatchNotFoundException {
        Match updatedMatch = matchService.updateMatch(id, matchMapper.toEntity(baseMatchDTO));
        return matchMapper.toDTO(updatedMatch);
    }

    public MatchDTO getMatch(Long id) throws MatchNotFoundException {
        Match match = matchService.getMatch(id);
        return matchMapper.toDTO(match);
    }

    public MatchDTO deleteMatch(Long id) throws IllegalArgumentException, MatchNotFoundException {
        Match deletedMatch = matchService.deleteMatch(id);
        return matchMapper.toDTO(deletedMatch);
    }
}

