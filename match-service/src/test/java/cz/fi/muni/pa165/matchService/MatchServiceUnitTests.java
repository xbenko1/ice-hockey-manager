package cz.fi.muni.pa165.matchService;

import cz.fi.muni.pa165.enums.MatchStatus;
import cz.fi.muni.pa165.exceptions.MatchNotFoundException;
import cz.fi.muni.pa165.matchService.repository.Match;
import cz.fi.muni.pa165.matchService.repository.MatchRepository;
import cz.fi.muni.pa165.matchService.service.MatchServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.web.reactive.function.client.WebClient;

import java.time.LocalDateTime;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@SpringBootTest
public class MatchServiceUnitTests {

    @Mock
    private MatchRepository matchRepository;

    @Mock
    private WebClient webClient;

    private MatchServiceImpl matchService;

    private final Match scheduledMatch =
            new Match(1L, MatchStatus.PLANNED, null, null, LocalDateTime.of(2025,6,6,16,20));

    private final Match playedMatch =
            new Match(2L, MatchStatus.TEAM2_WIN, 1, 5, LocalDateTime.of(2023,8,9,14,0));

    private final Match wrongMatch =
            new Match(null, null, -1, null, LocalDateTime.of(2025,6,6,16,20));



    @BeforeEach
    void setUp() {
        matchService = spy(new MatchServiceImpl(matchRepository, webClient));
        doReturn(true).when(matchService).amIAdmin();
    }


    @Test
    void scheduleMatch_Success() {
        Match expectedMatch = new Match();
        expectedMatch.setTeam1Score(2);

        when(matchRepository.save(any(Match.class))).thenReturn(expectedMatch);

        Match created = matchService.scheduleMatch(expectedMatch);

        assertNotNull(created);
        assertEquals(2, created.getTeam1Score());
        verify(matchRepository, times(1)).save(any(Match.class));
    }

    @Test
    void scheduleMatch_Fail_NullDTO() {
        assertThrows(IllegalArgumentException.class, () -> matchService.scheduleMatch(null));
        verify(matchRepository, times(0)).save(any(Match.class));
    }

    @Test
    void scheduleMatch_Fail_NotAdmin() {
        doReturn(false).when(matchService).amIAdmin();
        assertThrows(IllegalArgumentException.class, () -> matchService.scheduleMatch(playedMatch));
        verify(matchRepository, times(0)).save(any(Match.class));
    }

    @Test
    void getMatch_Success() {
        Match expectedMatch = new Match();
        expectedMatch.setId(1L);
        when(matchRepository.findById(1L)).thenReturn(Optional.of(expectedMatch));

        Match actualMatch = assertDoesNotThrow(() -> matchService.getMatch(1L));
        assertNotNull(actualMatch);
        assertEquals(expectedMatch.getId(), actualMatch.getId());
    }

    @Test
    void getMatch_NullId_ThrowsIllegalArgumentException() {
        IllegalArgumentException thrown = assertThrows(
                IllegalArgumentException.class,
                () -> matchService.getMatch(null),
                "Expected getMatch(null) to throw, but it didn't"
        );

        assertTrue(thrown.getMessage().contains(MatchServiceImpl.ID_NOT_NULL_ERROR));
    }

    @Test
    void getMatch_NonExistingId_ThrowsMatchNotFoundException() {
        Long nonExistingId = Long.MAX_VALUE;
        when(matchRepository.findById(nonExistingId)).thenReturn(Optional.empty());

        assertThrows(
                MatchNotFoundException.class,
                () -> matchService.getMatch(nonExistingId),
                "Expected getMatch(nonExistingId) to throw MatchNotFoundException, but it didn't"
        );
    }

    @Test
    void updateMatch_Success() throws MatchNotFoundException {
        Match expectedMatch = new Match();
        expectedMatch.setTeam1Score(1);
        expectedMatch.setTeam2Score(4);
        when(matchRepository.existsById(1L)).thenReturn(true);
        when(matchRepository.save(any(Match.class))).thenReturn(expectedMatch);

        Match updatedMatch = matchService.updateMatch(1L, expectedMatch);

        assertNotNull(updatedMatch);
        assertEquals(1, updatedMatch.getTeam1Score());
        assertEquals(4, updatedMatch.getTeam2Score());
        verify(matchRepository, times(1)).save(any(Match.class));
    }

    @Test
    void updateMatch_Fail_NotFound() {
        Long nonExistingId = 99L;
        when(matchRepository.findById(nonExistingId)).thenReturn(Optional.empty());

        assertThrows(MatchNotFoundException.class, () -> matchService.updateMatch(nonExistingId, playedMatch));
    }

    @Test
    void updateMatch_Fail_NullId() {
        assertThrows(IllegalArgumentException.class, () -> matchService.updateMatch(null, playedMatch));
        verify(matchRepository, times(0)).findById(any());
        verify(matchRepository, times(0)).save(any(Match.class));
    }


    @Test
    void deleteMatch_Success() throws MatchNotFoundException {
        Long existingId = 1L;
        Match existingMatch = new Match();
        existingMatch.setId(existingId);
        when(matchRepository.findById(existingId)).thenReturn(Optional.of(existingMatch));

        // Test the deletion
        matchService.deleteMatch(existingId);

        // Verify that the delete method of the repository was called once with the existing match
        verify(matchRepository, times(1)).delete(existingMatch);
    }

    @Test
    void deleteMatch_Fail_NotFound() {
        // Mock data
        Long nonExistingId = 99L;
        when(matchRepository.findById(nonExistingId)).thenReturn(Optional.empty());

        // Test the failure scenario
        assertThrows(MatchNotFoundException.class, () -> matchService.deleteMatch(nonExistingId));

        // Verify that the delete method of the repository was not called
        verify(matchRepository, times(0)).deleteById(nonExistingId);
    }

}

