package cz.fi.muni.pa165.matchService;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.fi.muni.pa165.dtos.BaseMatchDTO;
import cz.fi.muni.pa165.dtos.MatchDTO;
import cz.fi.muni.pa165.enums.MatchStatus;
import cz.fi.muni.pa165.exceptions.MatchNotFoundException;
import cz.fi.muni.pa165.matchService.facade.MatchFacade;
import cz.fi.muni.pa165.matchService.rest.MatchController;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDateTime;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(MatchController.class)
class MatchControllerUnitTests {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private MatchFacade matchFacade;

    @Autowired
    private ObjectMapper objectMapper;

    MatchDTO matchOne = new MatchDTO(
            1L,
            MatchStatus.TEAM1_WIN,
            3,
            2,
            LocalDateTime.of(2024,4,1,18,0)
    );

    BaseMatchDTO matchOneBaseDTO = new BaseMatchDTO(
            MatchStatus.TEAM1_WIN,
            3,
            2,
            LocalDateTime.of(2024,4,1,18,0)
    );


    MatchDTO matchTwo = new MatchDTO(
            2L,
            MatchStatus.PLANNED,
            null,
            null,
            LocalDateTime.of(2025,8,7,18,0)
    );

    BaseMatchDTO matchTwoBaseDTO = new BaseMatchDTO(
            MatchStatus.PLANNED,
            null,
            null,
            LocalDateTime.of(2025,8,7,18,0)
    );

    List<BaseMatchDTO> invalidDTOs = List.of(
            new BaseMatchDTO(null, 3, 2, LocalDateTime.of(2024,2,1,17,30)), // Invalid status
            new BaseMatchDTO(MatchStatus.TEAM2_WIN, -1, 2, LocalDateTime.of(2024,2,1,17,30)), // Invalid team 1 score
            new BaseMatchDTO(MatchStatus.TEAM1_WIN, 3, -1, LocalDateTime.of(2024,2,1,17,30)), // Invalid team 2 score
            new BaseMatchDTO(MatchStatus.PLANNED, 3, 2, null)  // Invalid time
    );

//    @Test
//    void getAllMatches_Success() throws Exception {
//        // Mocking service response
//        given(matchFacade.getAllMatches()).willReturn(List.of(matchOne, matchTwo));
//
//        mockMvc.perform(get("/api/matches"))
//                .andExpect(status().isOk())
//                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
//                .andExpect(jsonPath("$", hasSize(2)))
//                .andExpect(jsonPath("$[0].id", is(1)))
//                .andExpect(jsonPath("$[1].id", is(2)));
//    }
//
//    @Test
//    void getPlayedMatches_Success() throws Exception {
//        // Mocking service response
//        List<MatchDTO> playedMatches = List.of(matchOne);
//        given(matchFacade.getPlayedMatches()).willReturn(playedMatches);
//
//        mockMvc.perform(get("/api/matches/played"))
//                .andExpect(status().isOk())
//                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
//                .andExpect(jsonPath("$", hasSize(playedMatches.size())));
//    }
//
//    @Test
//    void getScheduledMatches_Success() throws Exception {
//        // Mocking service response
//        List<MatchDTO> scheduledMatches = List.of(matchTwo);
//        given(matchFacade.getScheduledMatches()).willReturn(scheduledMatches);
//
//        mockMvc.perform(get("/api/matches/scheduled"))
//                .andExpect(status().isOk())
//                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
//                .andExpect(jsonPath("$", hasSize(scheduledMatches.size())));
//    }
//
//    @Test
//    void getMatch_Success() throws Exception, MatchNotFoundException {
//        Long matchId = 1L;
//        given(matchFacade.getMatch(matchId)).willReturn(matchOne);
//
//        mockMvc.perform(get("/api/matches/{id}", matchId))
//                .andExpect(status().isOk())
//                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
//                .andExpect(jsonPath("$.id", is(matchId.intValue())));
//    }
//
//    @Test
//    void getMatch_Fail_NotFound() throws Exception, MatchNotFoundException {
//        Long matchId = 99L; // Assuming this ID doesn't exist
//        given(matchFacade.getMatch(matchId)).willThrow(new MatchNotFoundException());
//
//        mockMvc.perform(get("/api/matches/{id}", matchId))
//                .andExpect(status().isNotFound());
//    }
//
//    @Test
//    void scheduleMatch_Success() throws Exception {
//        // Set up mock behavior for the matchFacade
//        when(matchFacade.scheduleMatch(any(BaseMatchDTO.class))).thenReturn(matchOne);
//
//        // Perform request with valid DTO and expect success
//        mockMvc.perform(post("/api/matches")
//                        .contentType(MediaType.APPLICATION_JSON)
//                        .content(objectMapper.writeValueAsString(matchOneBaseDTO)))
//                .andExpect(status().isCreated())
//                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
//                .andExpect(jsonPath("$.id").value(1))
//                .andExpect(jsonPath("$.status").value("TEAM1_WIN"))
//                .andExpect(jsonPath("$.team1Score").value(3))
//                .andExpect(jsonPath("$.team2Score").value(2))
//                .andExpect(jsonPath("$.matchTime").value("2024-04-01T18:00:00"));
//    }
//
//    @Test
//    void scheduleMatch_InvalidAttributes() throws Exception {
//        // Set up mock behavior for the playerService
//        when(matchFacade.scheduleMatch(any(BaseMatchDTO.class))).thenReturn(matchOne);
//
//        for (BaseMatchDTO invalidDTO : invalidDTOs) {
//            mockMvc.perform(post("/api/matches")
//                            .contentType(MediaType.APPLICATION_JSON)
//                            .content(objectMapper.writeValueAsString(invalidDTO)))
//                    .andExpect(status().isBadRequest());
//        }
//    }
//
//    @Test
//    void updateMatch_Success() throws MatchNotFoundException, Exception {
//        Long matchId = 1L;
//        BaseMatchDTO validMatchDTO = new BaseMatchDTO(
//                MatchStatus.TEAM1_WIN,
//                4, 3,
//                LocalDateTime.of(2024, 8, 7, 18, 0));
//
//        MatchDTO updatedMatch = new MatchDTO(1L,
//                MatchStatus.TEAM1_WIN,
//                4, 3,
//                LocalDateTime.of(2024, 8, 7, 18, 0));
//
//        when(matchFacade.updateMatch(eq(matchId), any(BaseMatchDTO.class))).thenReturn(updatedMatch);
//
//        mockMvc.perform(put("/api/match/{id}", matchId)
//                        .contentType(MediaType.APPLICATION_JSON)
//                        .content(objectMapper.writeValueAsString(validMatchDTO)))
//                .andExpect(status().isOk())
//                .andExpect(jsonPath("$.status", is("TEAM1_WIN")))
//                .andExpect(jsonPath("$.team1Score", is(4)))
//                .andExpect(jsonPath("$.team2Score", is(3)));
//    }
//
//    @Test
//    void updateMatch_MatchNotFound() throws Exception, MatchNotFoundException {
//        Long matchId = 99L;
//        BaseMatchDTO matchDTO = new BaseMatchDTO(
//                MatchStatus.TEAM1_WIN,
//                4, 3,
//                LocalDateTime.of(2024, 8, 7, 18, 0));
//
//        when(matchFacade.updateMatch(eq(matchId), any(BaseMatchDTO.class))).thenThrow(new MatchNotFoundException());
//
//        mockMvc.perform(put("/api/match/{id}", matchId)
//                        .contentType(MediaType.APPLICATION_JSON)
//                        .content(objectMapper.writeValueAsString(matchDTO)))
//                .andExpect(status().isNotFound());
//    }
//
//    @Test
//    void updateMatch_InvalidAttributes() throws Exception {
//        Long matchId = 1L;
//
//        for (BaseMatchDTO invalidDTO : invalidDTOs) {
//            mockMvc.perform(put("/api/match/{id}", matchId)
//                            .contentType(MediaType.APPLICATION_JSON)
//                            .content(objectMapper.writeValueAsString(invalidDTO)))
//                    .andExpect(status().isBadRequest());
//        }
//    }
//    @Test
//    void deleteMatch_Success() throws Exception, MatchNotFoundException {
//        Long matchId = 1L;
//
//        when(matchFacade.deleteMatch(matchId)).thenReturn(matchOne);
//
//        mockMvc.perform(delete("/api/match/{id}", matchId))
//                .andExpect(status().isOk())
//                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
//                .andExpect(jsonPath("$.id", is(matchId.intValue())));
//
//        verify(matchFacade).deleteMatch(matchId);
//    }
//
//    @Test
//    void deleteMatch_NotFound() throws Exception, MatchNotFoundException {
//        Long matchId = 99L;
//        when(matchFacade.deleteMatch(matchId)).thenThrow(new MatchNotFoundException());
//
//        mockMvc.perform(delete("/api/match/{id}", matchId))
//                .andExpect(status().isNotFound());
//
//        verify(matchFacade).deleteMatch(matchId);
//    }



}

