package cz.fi.muni.pa165.matchService;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.fi.muni.pa165.dtos.BaseMatchDTO;
import cz.fi.muni.pa165.enums.MatchStatus;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import java.time.LocalDateTime;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
class MatchServiceIT {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

//    @Test
//    void getAllMatches() throws Exception {
//        mockMvc.perform(get("/api/matches"))
//                .andExpect(status().isOk())
//                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
//                .andExpect(jsonPath("$").isArray());
//    }
//
//    @Test
//    void getMatch_NotFound() throws Exception {
//        mockMvc.perform(get("/api/matches/{id}", Long.MAX_VALUE))
//                .andExpect(status().isNotFound());
//    }
//    @Test
//    void match_LifeCycle_CreateUpdateDelete() throws Exception {
//        // Create a new match
//        BaseMatchDTO newMatch = new BaseMatchDTO( MatchStatus.PLANNED, 0, 0, LocalDateTime.of(2024, 4, 10, 18, 0));
//        String matchJson = objectMapper.writeValueAsString(newMatch);
//
//        String matchLocation = mockMvc.perform(post("/api/matches")
//                        .contentType(MediaType.APPLICATION_JSON)
//                        .content(matchJson))
//                .andExpect(status().isCreated())
//                .andReturn().getResponse().getHeader("Location");
//
//        // Extract match ID from the location URL
//        assert matchLocation != null;
//        String matchId = matchLocation.substring(matchLocation.lastIndexOf('/') + 1);
//
//        // Update the match to change the score
//        newMatch.setTeam1Score(4);
//        newMatch.setTeam2Score(3);
//        mockMvc.perform(put("/api/match/{id}", matchId)
//                        .contentType(MediaType.APPLICATION_JSON)
//                        .content(objectMapper.writeValueAsString(newMatch)))
//                .andExpect(status().isOk())
//                .andExpect(jsonPath("$.team1Score", is(4)))
//                .andExpect(jsonPath("$.team2Score", is(3)));
//
//        // Successfully delete the match
//        mockMvc.perform(delete("/api/match/{id}", matchId))
//                .andExpect(status().isOk());
//
//        // Test that it's deleted
//        mockMvc.perform(get("/api/matches/{id}", matchId))
//                .andExpect(status().isNotFound());
//    }
}
