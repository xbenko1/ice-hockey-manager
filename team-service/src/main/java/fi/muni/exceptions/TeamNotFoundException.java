package fi.muni.exceptions;

public class TeamNotFoundException extends Throwable {
    public TeamNotFoundException(String message) {
        super(message);
    }
}
