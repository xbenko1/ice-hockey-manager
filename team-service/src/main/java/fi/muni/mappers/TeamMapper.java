package fi.muni.mappers;

import fi.muni.repository.TeamEntity;
import cz.fi.muni.pa165.dtos.BaseTeamDTO;
import cz.fi.muni.pa165.dtos.TeamDTO;
import org.mapstruct.Mapper;


@Mapper(componentModel = "spring")
public interface TeamMapper {

    TeamDTO toDTO(TeamEntity team);
    TeamEntity toEntity(TeamDTO dto);
    TeamEntity toEntity(BaseTeamDTO baseDto);
}