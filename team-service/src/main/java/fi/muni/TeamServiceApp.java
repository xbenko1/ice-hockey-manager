package fi.muni;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class TeamServiceApp {
    private static final Logger logger = LoggerFactory.getLogger(TeamServiceApp.class);

    // Retrieve server port and context-path from application.yml

    @Value("${server.port:8082}")
    private String serverPort;

    @Value("${springdoc.api-docs.path:/openapi}")
    private String apiDocsPath;

    @Value("${springdoc.swagger-ui.path:/swagger-ui.html}")
    private String swaggerUiPath;

    public static void main(String[] args) {
        ApplicationContext ctx = SpringApplication.run(TeamServiceApp.class, args);
    }

    @Bean
    CommandLineRunner run() {
        return args -> {
            String baseUrl = "http://localhost:" + serverPort;
            logger.info("Team Service Application has started successfully.");
            logger.info("API Documentation: {}", baseUrl + apiDocsPath);
            logger.info("Swagger UI: {}", baseUrl + swaggerUiPath);
        };
    }
}