package fi.muni.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface TeamRepository extends JpaRepository<TeamEntity, Long> {

    List<TeamEntity> findByManagerIdIsNull();

    @Query("SELECT t FROM TeamEntity t WHERE t.freeSpots > 0")
    List<TeamEntity> findAllAssignableTeams();
}