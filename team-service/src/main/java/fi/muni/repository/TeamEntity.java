package fi.muni.repository;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import jakarta.validation.constraints.PositiveOrZero;
import lombok.*;

@Schema(description = "Represents a Hockey Team in the system.")
@Entity
@Table(name = "team")
public class TeamEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Schema(description = "The ID of the team.", example = "1")
    private Long id;

    @NotBlank(message = "Name of team must not be blank")
    @Schema(description = "The name of the team.", example = "NewYork Rangers")
    private String name;

    @PositiveOrZero(message = "Budget must be positive or zero")
    @Schema(description = "The budget of a team", example = "60_000")
    private Integer budget;

    @PositiveOrZero(message = "Free spots must be positive or zero")
    @Schema(description = "The free spots for new players", example = "6")
    private Integer freeSpots;

    @Schema(description = "Manager ID which owns the team", example = "12")
    private Long managerId;

    public TeamEntity() { }

    public TeamEntity(long id, String name, Integer budget, Integer freeSpots, Long managerId) {
        this.id = id;
        this.name = name;
        this.budget = budget;
        this.freeSpots = freeSpots;
        this.managerId = managerId;
    }

    public long getId() {
        return id;
    }

    public void setId(Long id) { this.id = id; }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getBudget() {
        return budget;
    }

    public void setBudget(Integer budget) {
        this.budget = budget;
    }

    public Integer getFreeSpots() {
        return freeSpots;
    }

    public void setFreeSpots(Integer freeSpots) {
        this.freeSpots = freeSpots;
    }

    public Long getManagerId() {
        return managerId;
    }

    public void setManagerId(Long managerId) {
        this.managerId = managerId;
    }
}
