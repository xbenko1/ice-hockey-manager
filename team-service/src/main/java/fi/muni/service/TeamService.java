package fi.muni.service;

import fi.muni.repository.TeamEntity;
import fi.muni.exceptions.TeamDeleteException;
import fi.muni.exceptions.TeamNotFoundException;

import java.util.List;

public interface TeamService {

    List<TeamEntity> getAllTeams();

    List<TeamEntity> getAllAssignableTeams();

    List<TeamEntity> getAllManagerLessTeams();

    TeamEntity getRandomTeam();

    TeamEntity getTeam(Long id) throws IllegalArgumentException, TeamNotFoundException;

    TeamEntity createTeam(TeamEntity team) throws IllegalArgumentException;

    TeamEntity updateTeam(Long id, TeamEntity team) throws IllegalArgumentException, TeamNotFoundException;
    TeamEntity pickTeam(Long teamId) throws IllegalArgumentException, TeamNotFoundException;

    TeamEntity deleteTeam(Long id) throws IllegalArgumentException, TeamNotFoundException, TeamDeleteException;
}
