package fi.muni.service;

import fi.muni.repository.TeamEntity;
import fi.muni.exceptions.TeamDeleteException;
import fi.muni.exceptions.TeamNotFoundException;
import fi.muni.repository.TeamRepository;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.server.resource.authentication.BearerTokenAuthentication;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.Random;

@Service
public class TeamServiceImpl implements TeamService {

    static public String ID_NOT_NULL_ERROR = "Id can not be null";

    private final TeamRepository teamRepository;

    private final WebClient webClient;

    @Autowired
    public TeamServiceImpl(TeamRepository teamRepository, WebClient webClient) {
        this.teamRepository = teamRepository;
        this.webClient = webClient;
    }

    @Transactional
    @Override
    public List<TeamEntity> getAllTeams() {
        return teamRepository.findAll();
    }

    @Transactional
    @Override
    public List<TeamEntity> getAllAssignableTeams() {
        return teamRepository.findAllAssignableTeams();
    }

    @Transactional
    @Override
    public List<TeamEntity> getAllManagerLessTeams() {
        return teamRepository.findByManagerIdIsNull();
    }

    @Transactional
    @Override
    public TeamEntity getRandomTeam() {
        var teams = teamRepository.findAll();

        if (!teams.isEmpty()) {
            int randomIndex = new Random().nextInt(teams.size());
            return teams.get(randomIndex);
        } else {
            return null;
        }
    }

    @Transactional
    @Override
    public TeamEntity getTeam(Long id) throws IllegalArgumentException, TeamNotFoundException {
        if (id == null) {
            throw new IllegalArgumentException(ID_NOT_NULL_ERROR);
        }

        return teamRepository.findById(id)
                .orElseThrow(() -> new TeamNotFoundException("Team with id " + id + " was not found."));
    }

    @Transactional
    @Override
    public TeamEntity createTeam(TeamEntity team) throws IllegalArgumentException {
        if (!amIAdmin()) {
            throw new IllegalArgumentException("Only admin can create a player");
        }
        if (team == null) {
            throw new IllegalArgumentException(ID_NOT_NULL_ERROR);
        }

        // TODO check teamId if TEAM exists when we get to JPA
        return teamRepository.save(team);
    }

    @Transactional
    @Override
    public TeamEntity updateTeam(Long id, TeamEntity team) throws IllegalArgumentException, TeamNotFoundException {
        if (!amIAdmin()) {
            throw new IllegalArgumentException("Only admin can create a player");
        }
        if (id == null) {
            throw new IllegalArgumentException(ID_NOT_NULL_ERROR);
        }
        if (team == null) {
            throw new IllegalArgumentException("TeamEntity can not be null");
        }
        if (!teamRepository.existsById(id)) {
            throw new TeamNotFoundException("Team with id " + id + " was not found.");
        }
        team.setId(id);

        return teamRepository.save(team);
    }

    @Transactional
    @Override
    public TeamEntity pickTeam(Long teamId) throws IllegalArgumentException, TeamNotFoundException {
        if (teamId == null) {
            throw new IllegalArgumentException(ID_NOT_NULL_ERROR);
        }
        TeamEntity team = getTeam(teamId);
        if (team.getManagerId() != null) {
            throw new IllegalArgumentException("Team already has a manager");
        }
        Long managerId = myId();
        team.setManagerId(managerId);
        return teamRepository.save(team);
    }

    @Transactional
    @Override
    public TeamEntity deleteTeam(Long id) throws IllegalArgumentException, TeamNotFoundException, TeamDeleteException {
        if (!amIAdmin()) {
            throw new IllegalArgumentException("Only admin can create a player");
        }
        if (id == null) {
            throw new IllegalArgumentException(ID_NOT_NULL_ERROR);
        }
        var teamToDelete = getTeam(id);
        if (teamToDelete.getFreeSpots() < 20) {
            throw new TeamDeleteException();
        }
        teamRepository.delete(teamToDelete);
        return teamToDelete;

    }

    public boolean amIAdmin() {
        String jwt = extractJwtToken();
        Mono<Boolean> isAdmin = webClient.get()
                .uri("api/users/amIAdmin")
                .header("Authorization", "Bearer " + jwt)
                .retrieve()
                .bodyToMono(Boolean.class);
        return Boolean.TRUE.equals(isAdmin.block());
    }

    public Long myId() {
        String jwt = extractJwtToken();
        Mono<Long> myId = webClient.get()
                .uri("api/users/myId")
                .header("Authorization", "Bearer " + jwt)
                .retrieve()
                .bodyToMono(Long.class);
        return myId.block();
    }

    private String extractJwtToken() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        try {
            if (authentication instanceof BearerTokenAuthentication) {
                BearerTokenAuthentication jwtToken = (BearerTokenAuthentication) authentication;
                return jwtToken.getToken().getTokenValue();
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return "";
    }
}
