package fi.muni.rest;

import fi.muni.exceptions.TeamDeleteException;
import fi.muni.exceptions.TeamNotFoundException;
import fi.muni.facade.TeamFacade;
import cz.fi.muni.pa165.dtos.BaseTeamDTO;
import cz.fi.muni.pa165.dtos.TeamDTO;
import fi.muni.models.ErrorMessage;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.servers.Server;
import io.swagger.v3.oas.annotations.servers.ServerVariable;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@OpenAPIDefinition(
        info = @Info(title = "Team API",
                version = "1.0",
                description = """
                        API for managing teams in the ice hockey manager system
                        API defines following operations:
                        - Get all teams
                        - Get all assignable teams
                        - Get random team
                        - Get team
                        - Create team
                        - Update team
                        - Delete team
                        """
        ),
        servers = @Server(description = "team server", url = "{scheme}://{server}:{port}", variables = {
                @ServerVariable(name = "scheme", allowableValues = {"http", "https"}, defaultValue = "http"),
                @ServerVariable(name = "server", defaultValue = "localhost"),
                @ServerVariable(name = "port", defaultValue = "8082"),
        }))

@Tag(name = "Team", description = "tag.team.description")
@RequestMapping(path = "/api", produces = MediaType.APPLICATION_JSON_VALUE)
public class TeamController {

    private final TeamFacade teamFacade;
    private static final Logger logger = LoggerFactory.getLogger(TeamController.class);

    @Autowired
    public TeamController(TeamFacade teamFacade) {
        this.teamFacade = teamFacade;
    }

    @Operation(summary = "operation.getAllTeams.summary",
            responses = @ApiResponse(responseCode = "200", description = "operation.getAllTeams.response.description",
                    content = @Content(schema = @Schema(implementation = TeamDTO.class))))
    @GetMapping("/teams")
    public ResponseEntity<List<TeamDTO>> getAllTeams() {
        logger.info("Fetching all teams");
        var teams = teamFacade.getAllTeams();
        return ResponseEntity.ok(teams);
    }

    @Operation(summary = "operation.getAllAssignableTeams.summary",
            responses = @ApiResponse(responseCode = "200", description = "operation.getAllAssignableTeams.description",
                    content = @Content(schema = @Schema(implementation = TeamDTO.class))))
    @GetMapping("/teams/assignable-teams")
    public ResponseEntity<List<TeamDTO>> getAllAssignableTeams() {
        logger.info("Fetching all assignable teams");
        var assignableTeams = teamFacade.getAllAssignableTeams();
        return ResponseEntity.ok(assignableTeams);
    }

    @Operation(summary = "operation.getAllManagerLessTeams.summary",
            responses = @ApiResponse(responseCode = "200", description = "operation.getAllManagerLessTeams.description",
                    content = @Content(schema = @Schema(implementation = TeamDTO.class))))
    @GetMapping("/teams/manager-less-teams")
    public ResponseEntity<List<TeamDTO>> getAllManagerLessTeams() {
        logger.info("Fetching all manager less teams");
        var assignableTeams = teamFacade.getAllManagerLessTeams();
        return ResponseEntity.ok(assignableTeams);
    }

    @Operation(summary = "operation.getRandomTeam.summary",
            responses =
            @ApiResponse(responseCode = "200", description = "operation.getRandomTeam.description",
                    content = @Content(schema = @Schema(implementation = TeamDTO.class)))
    )
    @GetMapping("/teams/random")
    public ResponseEntity<TeamDTO> getRandomTeam() {
        logger.info("Fetching a random team");
        var randomTeam = teamFacade.getRandomTeam();
        return ResponseEntity.ok(randomTeam);
    }

    @Operation(summary = "operation.getTeam.summary",
            responses = {
                    @ApiResponse(responseCode = "200", description = "operation.getTeam.found.description",
                            content = @Content(schema = @Schema(implementation = TeamDTO.class))),
                    @ApiResponse(responseCode = "404", description = "operation.getTeam.notfound.description")
            })
    @GetMapping("/teams/{id}")
    @CrossOrigin("*")
    public ResponseEntity<TeamDTO> getTeam(@PathVariable Long id) {
        try {
            logger.info("Fetching team with id={}", id);
            var team = teamFacade.getTeam(id);
            return ResponseEntity.ok(team);
        } catch (TeamNotFoundException e) {
            logger.warn("Team not found with id={}", id);
            return ResponseEntity.notFound().build();
        }
    }

    @Operation(summary = "operation.createTeam.summary",
            responses = @ApiResponse(responseCode = "201", description = "operation.createTeam.description",
                    content = @Content(schema = @Schema(implementation = TeamDTO.class))))
    @PostMapping("/teams")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<?> createTeam(@Valid @RequestBody BaseTeamDTO baseTeamDTO) {
        logger.info("Creating new team with name {} {} ", baseTeamDTO.getName(), baseTeamDTO.getBudget());
        var newTeam = teamFacade.createTeam(baseTeamDTO);
        var location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
                .buildAndExpand(newTeam.getId()).toUri();
        return ResponseEntity.created(location).body(newTeam);
    }

    @Operation(summary = "operation.updateTeam.summary",
            responses = {
                    @ApiResponse(responseCode = "200", description = "operation.updateTeam.updated.description",
                            content = @Content(schema = @Schema(implementation = TeamDTO.class))),
                    @ApiResponse(responseCode = "400", description = "operation.updateTeam.validation.description"),
                    @ApiResponse(responseCode = "404", description = "operation.updateTeam.notfound.description")
            })
    @PutMapping("/teams/{id}")
    @CrossOrigin("*")
    public ResponseEntity<?> updateTeam(@PathVariable Long id,
                                          @Valid @RequestBody BaseTeamDTO baseTeamDTO,
                                          BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            var errorMessages = bindingResult.getFieldErrors().stream()
                    .map(fe -> fe.getField() + " - " + fe.getDefaultMessage())
                    .collect(Collectors.joining(", "));
            logger.error("Validation errors while updating team {}: {}", id, errorMessages);
            return ResponseEntity.badRequest().body(new ErrorMessage(errorMessages));
        }

        try {
            logger.info("Updating team with ID: {}", id);
            var updatedTeam = teamFacade.updateTeam(id, baseTeamDTO);
            return ResponseEntity.ok(updatedTeam);
        } catch (IllegalArgumentException e) {
            logger.error("Error updating team {}: {}", id, e.getMessage());
            return ResponseEntity.badRequest().body(new ErrorMessage(e.getMessage()));
        } catch (TeamNotFoundException e) {
            logger.warn("Team not found with id={}", id);
            return ResponseEntity.notFound().build();
        }
    }

    @Operation(summary = "operation.deleteTeam.summary",
            responses = {
                    @ApiResponse(responseCode = "200", description = "operation.deleteTeam.deleted.description",
                            content = @Content(schema = @Schema(implementation = TeamDTO.class))),
                    @ApiResponse(responseCode = "404", description = "operation.deleteTeam.notfound.description"),
                    @ApiResponse(responseCode = "409", description = "operation.deleteTeam.conflict.description")
            })
    @DeleteMapping("/teams/{id}")
    @CrossOrigin("*")
    public ResponseEntity<?> deleteTeam(@PathVariable Long id) {
        try {
            logger.info("Deleting teams with ID: {}", id);
            var team = teamFacade.deleteTeam(id);
            return ResponseEntity.ok(team);
        } catch (TeamNotFoundException e) {
            logger.error("Team not found with id={}", id);
            return ResponseEntity.notFound().build();
        } catch (TeamDeleteException e) {
            logger.warn("Tried to delete team with players.");
            return ResponseEntity
                    .status(HttpStatus.CONFLICT)
                    .body(new ErrorMessage("Cannot delete team that contains one or more players."));
        }
    }

    @Operation(summary = "operation.pickTeam.summary",
            responses = {
                    @ApiResponse(responseCode = "200", description = "operation.pickTeam.set.description",
                            content = @Content(schema = @Schema(implementation = TeamDTO.class))),
                    @ApiResponse(responseCode = "404", description = "operation.pickTeam.notfound.description")
            })
    @PutMapping("/teams/pick/{id}")
    @CrossOrigin("*")
    public ResponseEntity<?> pickTeam(@PathVariable Long id) {
        try {
            logger.info("Setting manager to the team with ID: {}", id);
            var updatedTeam = teamFacade.pickTeam(id);
            return ResponseEntity.ok(updatedTeam);
        } catch (TeamNotFoundException e) {
            logger.error("Team not found with id={}", id);
            return ResponseEntity.notFound().build();
        }
    }
}