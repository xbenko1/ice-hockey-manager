package fi.muni.facade;

import cz.fi.muni.pa165.dtos.TeamDTO;
import fi.muni.exceptions.TeamDeleteException;
import fi.muni.exceptions.TeamNotFoundException;
import cz.fi.muni.pa165.dtos.BaseTeamDTO;
import fi.muni.mappers.TeamMapper;
import fi.muni.service.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class TeamFacadeImpl implements TeamFacade {

    private final TeamService teamService;
    private final TeamMapper teamMapper;

    @Autowired
    public TeamFacadeImpl(TeamService teamService, TeamMapper teamMapper){
        this.teamService = teamService;
        this.teamMapper = teamMapper;
    }

    @Override
    public List<TeamDTO> getAllTeams() {
        return  teamService.getAllTeams().stream()
                .map(teamMapper::toDTO)
                .collect(Collectors.toList());
    }

    @Override
    public List<TeamDTO> getAllAssignableTeams() {
        return  teamService.getAllAssignableTeams().stream()
                .map(teamMapper::toDTO)
                .collect(Collectors.toList());
    }

    @Override
    public List<TeamDTO> getAllManagerLessTeams() {
        return teamService.getAllManagerLessTeams().stream()
                .map(teamMapper::toDTO)
                .collect(Collectors.toList());
    }

    @Override
    public TeamDTO getRandomTeam() {
        var randomTeam = teamService.getRandomTeam();
        return  teamMapper.toDTO(randomTeam);
    }

    @Override
    public TeamDTO getTeam(Long id) throws IllegalArgumentException, TeamNotFoundException {
        var team = teamService.getTeam(id);
        return teamMapper.toDTO(team);
    }

    @Override
    public TeamDTO createTeam(BaseTeamDTO baseTeamDTO) throws IllegalArgumentException {
        var teamToCreate = teamMapper.toEntity(baseTeamDTO);
        var createdTeam = teamService.createTeam(teamToCreate);
        return teamMapper.toDTO(createdTeam);
    }

    @Override
    public TeamDTO updateTeam(Long id, BaseTeamDTO baseTeamDTO) throws IllegalArgumentException, TeamNotFoundException {
        var teamToUpdate = teamMapper.toEntity(baseTeamDTO);
        var updatedTeam = teamService.updateTeam(id, teamToUpdate);
        return teamMapper.toDTO(updatedTeam);
    }

    @Override
    public TeamDTO pickTeam(Long teamId) throws IllegalArgumentException, TeamNotFoundException {
        var teamWithManager = teamService.pickTeam(teamId);
        return teamMapper.toDTO(teamWithManager);
    }

    @Override
    public TeamDTO deleteTeam(Long id) throws IllegalArgumentException, TeamNotFoundException, TeamDeleteException {
        var deletedTeam = teamService.deleteTeam(id);
        return teamMapper.toDTO(deletedTeam);
    }
}
