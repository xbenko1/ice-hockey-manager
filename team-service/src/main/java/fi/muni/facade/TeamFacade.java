package fi.muni.facade;

import cz.fi.muni.pa165.dtos.TeamDTO;
import fi.muni.exceptions.TeamDeleteException;
import fi.muni.exceptions.TeamNotFoundException;
import cz.fi.muni.pa165.dtos.BaseTeamDTO;

import java.util.List;

public interface TeamFacade {

    List<TeamDTO> getAllTeams();

    List<TeamDTO> getAllAssignableTeams();

    List<TeamDTO> getAllManagerLessTeams();

    TeamDTO getRandomTeam();

    TeamDTO getTeam(Long id) throws IllegalArgumentException, TeamNotFoundException;

    TeamDTO createTeam(BaseTeamDTO baseTeamDTO) throws IllegalArgumentException;

    TeamDTO updateTeam(Long id, BaseTeamDTO baseTeamDTO) throws IllegalArgumentException, TeamNotFoundException;
    TeamDTO pickTeam(Long teamId) throws IllegalArgumentException, TeamNotFoundException;

    TeamDTO deleteTeam(Long id) throws IllegalArgumentException, TeamNotFoundException, TeamDeleteException;

}
