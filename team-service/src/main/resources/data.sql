INSERT INTO team (name, budget, free_spots, manager_id)
VALUES
('Carolina Hurricanes', 15000000, 18, 1),
('Florida Panthers', 170000000, 18, NULL),
('Pittsburgh Penguins', 12000000, 18, NULL),
('New York Rangers', 35000000, 21, NULL);