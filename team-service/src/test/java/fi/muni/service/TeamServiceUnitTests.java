package fi.muni.service;

import fi.muni.repository.TeamEntity;
import fi.muni.exceptions.TeamDeleteException;
import fi.muni.exceptions.TeamNotFoundException;
import fi.muni.repository.TeamRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.Optional;

import static org.hibernate.validator.internal.util.Contracts.assertNotNull;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@SpringBootTest(classes = TeamServiceUnitTests.class)
class TeamServiceUnitTests {

    @InjectMocks
    private TeamServiceImpl teamService;

    @Mock
    private TeamRepository teamRepository;

    @Mock
    private WebClient webClient;

    private final TeamEntity teamOne =
            new TeamEntity(1L,"Kometa", 420_000, 10,69L);

    @BeforeEach
    void setUp() {
        teamService = spy(new TeamServiceImpl(teamRepository, webClient));
        doReturn(true).when(teamService).amIAdmin();
    }

    @Test
    void createTeam_Success() {
        var newTeam = new TeamEntity(1L, "Test", 10_100, 20, 26L);

        when(teamRepository.save(any(TeamEntity.class))).thenReturn(newTeam);

        var created = teamService.createTeam(newTeam);

        assertNotNull(created);
        assertEquals("Test", created.getName());
        verify(teamRepository, times(1)).save(any(TeamEntity.class));
    }

    @Test
    void createTeam_Fail_NullDTO() {
        IllegalArgumentException thrown = assertThrows(
                IllegalArgumentException.class,
                () -> teamService.createTeam(null),
                "Expected createTeam(null) to throw, but it did not");

        assertTrue(thrown.getMessage().contains(TeamServiceImpl.ID_NOT_NULL_ERROR));
    }

    @Test
    void getTeam_Success() {
        var expectedTeam = new TeamEntity(1L, "Test", 10_100, 20, 26L);

        when(teamRepository.findById(1L)).thenReturn(Optional.of(expectedTeam));

        var actualTeam = assertDoesNotThrow(() -> teamService.getTeam(1L));
        assertNotNull(actualTeam);
        assertEquals(expectedTeam.getId(), actualTeam.getId());
    }

    @Test
    void updateTeam_Success() throws TeamNotFoundException {
        var expectedTeam = new TeamEntity(1L, "Updated Hello Team", 96, 0, 27L);

        when(teamRepository.existsById(1L)).thenReturn(true);
        when(teamRepository.save(any(TeamEntity.class))).thenReturn(expectedTeam);

        var updated = teamService.updateTeam(1L, expectedTeam);

        assertNotNull(updated);
        assertEquals("Updated Hello Team", updated.getName());
        verify(teamRepository, times(1)).save(any(TeamEntity.class));
    }

    @Test
    void updateTeam_Fail_NotFound() {
        Long nonExistingId = 99L;
        when(teamRepository.existsById(nonExistingId)).thenReturn(false);

        assertThrows(TeamNotFoundException.class, () -> teamService.updateTeam(nonExistingId, teamOne));
    }

    @Test
    void updateTeam_Fail_IllegalId() {
        assertThrows(IllegalArgumentException.class, () -> teamService.updateTeam(null, teamOne));
        verify(teamRepository, times(0)).findById(any());
        verify(teamRepository, times(0)).save(any(TeamEntity.class));
    }

    @Test
    void deleteTeam_Success() throws TeamDeleteException, TeamNotFoundException {
        var existingId = 1L;
        var existingTeam = new TeamEntity(existingId, "ToDeleteTeam", 10_000, 21, null);
        when(teamRepository.findById(existingId)).thenReturn(Optional.of(existingTeam));

        teamService.deleteTeam(existingId);

        verify(teamRepository, times(1)).delete(existingTeam);
    }

    @Test
    void deleteTeam_Fail_PlayerInTeam() {
        Long existingId = 2L;
        var existingTeam = new TeamEntity(1L, "ToDeleteTeam", 10_000, 10, null);
        when(teamRepository.findById(existingId)).thenReturn(Optional.of(existingTeam));

        assertThrows(TeamDeleteException.class, () -> teamService.deleteTeam(existingId));
        verify(teamRepository, times(0)).deleteById(existingId);
    }

    @Test
    void deleteTeam_Fail_NotFound() {
        Long nonExistingId = 99L;
        when(teamRepository.findById(nonExistingId)).thenReturn(Optional.empty());

        assertThrows(TeamNotFoundException.class, () -> teamService.deleteTeam(nonExistingId));
        verify(teamRepository, times(0)).deleteById(nonExistingId);
    }

    @Test
    void createNewTeam_Fail_NotAdmin() {
        doReturn(false).when(teamService).amIAdmin();
        assertThrows(IllegalArgumentException.class, () -> teamService.createTeam(teamOne));
        verify(teamRepository, times(0)).save(any(TeamEntity.class));
    }
}
