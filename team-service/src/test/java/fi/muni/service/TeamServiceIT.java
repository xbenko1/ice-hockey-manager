//package fi.muni.service;
//
//import com.fasterxml.jackson.databind.ObjectMapper;
//import cz.fi.muni.pa165.dtos.BaseTeamDTO;
//import org.junit.jupiter.api.Test;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.http.MediaType;
//import org.springframework.test.web.servlet.MockMvc;
//
//import static org.hamcrest.Matchers.is;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
//
//@SpringBootTest
//@AutoConfigureMockMvc
//class TeamServiceIT {
//
//    @Autowired
//    private MockMvc mockMvc;
//
//    @Autowired
//    private ObjectMapper objectMapper;
//
//    @Test
//    void getAllTeams() throws Exception {
//        mockMvc.perform(get("/api/teams"))
//                .andExpect(status().isOk())
//                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
//                .andExpect(jsonPath("$").isArray());
//    }
//    @Test
//    void getTeam_NotFound() throws Exception {
//        mockMvc.perform(get("/api/teams/{id}", Long.MAX_VALUE))
//                .andExpect(status().isNotFound());
//    }
//
//    @Test
//    void team_LifeCycle_CreateUpdateDelete() throws Exception {
//        var newTeam = new BaseTeamDTO("Kometa Brno", 69_420, 1, null);
//        String teamJson = objectMapper.writeValueAsString(newTeam);
//
//        String teamLocation = mockMvc.perform(post("/api/teams")
//                        .contentType(MediaType.APPLICATION_JSON)
//                        .content(teamJson))
//                .andExpect(status().isCreated())
//                .andReturn().getResponse().getHeader("Location");
//
//        assert teamLocation != null;
//        String teamId = teamLocation.substring(teamLocation.lastIndexOf('/') + 1);
//
//        newTeam.setManagerId(1L); // TODO UPDATE AFTER JPA
//        mockMvc.perform(put("/api/teams/{id}", teamId)
//                        .contentType(MediaType.APPLICATION_JSON)
//                        .content(objectMapper.writeValueAsString(newTeam)))
//                .andExpect(status().isOk())
//                .andExpect(jsonPath("$.managerId", is(1)));
//
//        mockMvc.perform(delete("/api/teams/{id}", teamId))
//                .andExpect(status().isConflict());
//
//        newTeam.setFreeSpots(20); // TODO UPDATE AFTER JPA
//        mockMvc.perform(put("/api/teams/{id}", teamId)
//                        .contentType(MediaType.APPLICATION_JSON)
//                        .content(objectMapper.writeValueAsString(newTeam)))
//                .andExpect(status().isOk())
//                .andExpect(jsonPath("$.freeSpots", is(20)));
//
//        mockMvc.perform(delete("/api/teams/{id}", teamId))
//                .andExpect(status().isOk());
//
//        mockMvc.perform(get("/api/teams/{id}", teamId))
//                .andExpect(status().isNotFound());
//    }
//}