//package fi.muni.rest;
//
//import com.fasterxml.jackson.databind.ObjectMapper;
//import fi.muni.repository.TeamEntity;
//import fi.muni.exceptions.TeamDeleteException;
//import fi.muni.exceptions.TeamNotFoundException;
//import fi.muni.facade.TeamFacade;
//import cz.fi.muni.pa165.dtos.BaseTeamDTO;
//import cz.fi.muni.pa165.dtos.TeamDTO;
//import org.junit.jupiter.api.Test;
//import org.mockito.InjectMocks;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
//import org.springframework.boot.test.mock.mockito.MockBean;
//import org.springframework.http.MediaType;
//import org.springframework.test.web.servlet.MockMvc;
//
//import java.util.List;
//
//import static org.hamcrest.Matchers.hasSize;
//import static org.hamcrest.Matchers.is;
//import static org.mockito.ArgumentMatchers.any;
//import static org.mockito.ArgumentMatchers.eq;
//import static org.mockito.BDDMockito.given;
//import static org.mockito.Mockito.verify;
//import static org.mockito.Mockito.when;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
//
//@WebMvcTest(TeamController.class)
//class TeamControllerUnitTests {
//
//    @Autowired
//    private MockMvc mockMvc;
//    @MockBean
//    private TeamFacade teamFacade;
//    @Autowired
//    private ObjectMapper objectMapper;
//    @InjectMocks
//    private TeamController teamController;
//
//    TeamEntity teamOne = new TeamEntity(
//            1L,
//            "Kometa Brno",
//            60_000,
//            5,
//            null
//    );
//    TeamDTO teamOneDTO = new TeamDTO(
//            1L,
//            "Kometa Brno",
//            60_000,
//            5,
//            null
//    );
//
//    TeamEntity teamTwo = new TeamEntity(
//            2L,
//            "Slavia Praha",
//            69_000,
//            0,
//            1L
//    );
//    TeamDTO teamTwoDTO = new TeamDTO(
//            2L,
//            "Slavia Praha",
//            69_000,
//            0,
//            1L
//    );
//
//    List<BaseTeamDTO> invalidDTOs = List.of(
//            new BaseTeamDTO(null, 10, 5, 25L),
//            new BaseTeamDTO("Test", -10, 5, 25L),
//            new BaseTeamDTO("Test", 10, -5, 25L)
//    );
//
//    @Test
//    void getAllTeams_Success() throws Exception {
//        given(teamFacade.getAllTeams()).willReturn(List.of(teamOneDTO, teamTwoDTO));
//
//        mockMvc.perform(get("/api/teams"))
//                .andExpect(status().isOk())
//                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
//                .andExpect(jsonPath("$", hasSize(2)))
//                .andExpect(jsonPath("$[0].id", is(1)))
//                .andExpect(jsonPath("$[1].id", is(2)));
//    }
//
//    @Test
//    void getAllAssignableTeams_Success() throws Exception {
//        given(teamFacade.getAllAssignableTeams()).willReturn(List.of(teamOneDTO));
//
//        mockMvc.perform(get("/api/teams/assignable-teams"))
//                .andExpect(status().isOk())
//                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
//                .andExpect(jsonPath("$", hasSize(1)))
//                .andExpect(jsonPath("$[0].id", is(1)));
//    }
//
//    @Test
//    void getAllManagerLessTeams_Success() throws Exception {
//        given(teamFacade.getAllManagerLessTeams()).willReturn(List.of(teamOneDTO));
//
//        mockMvc.perform(get("/api/teams/manager-less-teams"))
//                .andExpect(status().isOk())
//                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
//                .andExpect(jsonPath("$", hasSize(1)))
//                .andExpect(jsonPath("$[0].id", is(1)));
//    }
//
//    @Test
//    void getTeam_Success() throws Exception, TeamNotFoundException {
//        Long teamId = 1L;
//
//        given(teamFacade.getTeam(teamId)).willReturn(teamOneDTO);
//
//        mockMvc.perform(get("/api/teams/{id}", teamId))
//                .andExpect(status().isOk())
//                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
//                .andExpect(jsonPath("$.id", is(teamId.intValue())));
//    }
//
//    @Test
//    void getTeam_Fail_NotFound() throws Exception, TeamNotFoundException {
//        Long teamId = 99L; // Assuming this ID doesn't exist
//        given(teamFacade.getTeam(teamId)).willThrow(new TeamNotFoundException("Team was not found"));
//
//        mockMvc.perform(get("/api/players/{id}", teamId))
//                .andExpect(status().isNotFound());
//    }
//
//    @Test
//    void createTeam_Success() throws Exception {
//        when(teamFacade.createTeam(any(BaseTeamDTO.class))).thenReturn(teamTwoDTO);
//
//        mockMvc.perform(post("/api/teams")
//                        .contentType(MediaType.APPLICATION_JSON)
//                        .content(objectMapper.writeValueAsString(teamTwoDTO)))
//                .andExpect(status().isCreated());
//    }
//
//    @Test
//    void createTeam_InvalidAttributes() throws Exception {
//        when(teamFacade.createTeam(any(BaseTeamDTO.class))).thenReturn(teamOneDTO);
//
//        for (var invalidDTO : invalidDTOs) {
//            mockMvc.perform(post("/api/teams")
//                            .contentType(MediaType.APPLICATION_JSON)
//                            .content(objectMapper.writeValueAsString(invalidDTO)))
//                    .andExpect(status().isBadRequest());
//        }
//    }
//
//    @Test
//    void updateTeam_Success() throws Exception, TeamNotFoundException  {
//        Long teamId = 1L;
//        var validTeamDTO  = new BaseTeamDTO(
//                "Test",
//                20_000,
//                0,
//                10L);
//
//        var updatedTeamDTO = new TeamDTO(
//                1L,
//                "Test",
//                50_000,
//                0,
//                10L);
//
//        when(teamFacade.updateTeam(eq(teamId), any(BaseTeamDTO.class))).thenReturn(updatedTeamDTO);
//
//        mockMvc.perform(put("/api/teams/{id}", teamId)
//                        .contentType(MediaType.APPLICATION_JSON)
//                        .content(objectMapper.writeValueAsString(validTeamDTO)))
//                .andExpect(status().isOk())
//                .andExpect(jsonPath("$.budget", is(50_000)));
//    }
//
//    @Test
//    void deleteTeam_Success() throws Exception, TeamDeleteException, TeamNotFoundException {
//        Long teamID = 1L;
//
//        when(teamFacade.deleteTeam(teamID)).thenReturn(teamOneDTO);
//
//        mockMvc.perform(delete("/api/teams/{id}", teamID))
//                .andExpect(status().isOk())
//                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
//                .andExpect(jsonPath("$.id", is(teamID.intValue())));
//
//        verify(teamFacade).deleteTeam(teamID);
//    }
//}