package cz.fi.muni.pa165.userservice.service;

import cz.fi.muni.pa165.dtos.*;
import cz.fi.muni.pa165.userservice.data.User;
import cz.fi.muni.pa165.enums.UserRole;
import cz.fi.muni.pa165.exceptions.ResourceNotFoundException;
import cz.fi.muni.pa165.userservice.mappers.UserMapper;
import cz.fi.muni.pa165.userservice.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.server.resource.authentication.BearerTokenAuthentication;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.security.core.Authentication;

import java.util.ArrayList;
import java.util.List;

@Transactional
@Service
public class UserServiceImpl implements UserService {
    private final UserMapper userMapper;
    private final UserRepository userRepository;

    @Autowired
    public UserServiceImpl(final UserMapper userMapper, final UserRepository userRepository) {
        this.userMapper = userMapper;
        this.userRepository = userRepository;
    }

    @Override
    public UserDTO findUserByEmail(String email) throws IllegalArgumentException, ResourceNotFoundException {
        if (email == null) {
            throw new IllegalArgumentException("Email cannot be null");
        }
        return userMapper.userToUserDto(userRepository.findByEmail(email)
                .orElseThrow(() -> new ResourceNotFoundException("User with email " + email + " not found")));
    }

    @Override
    public UserDTO findUserById(Long id) throws IllegalArgumentException, ResourceNotFoundException {
        if (id == null) {
            throw new IllegalArgumentException("Id cannot be null");
        }
        return userMapper.userToUserDto(userRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("User with id " + id + " not found")));
    }

    @Override
    public List<UserDTO> getAllUsers() {
        return userMapper.usersToUserDtos(userRepository.findAll());
    }

    @Override
    public void deleteUser(Long userId) throws IllegalArgumentException, ResourceNotFoundException {
        if (userId == null) {
            throw new IllegalArgumentException("Id cannot be null");
        }
        if (!isAuthenticatedUserAdmin()) {
            throw new IllegalArgumentException("User is not an admin");
        }
        User user = userRepository.findById(userId)
                .orElseThrow(() -> new ResourceNotFoundException("User with id " + userId + " not found"));
        userRepository.delete(user);
    }


    @Override
    public void updateUserToAdmin(Long userId) {
        if (userId == null) {
            throw new IllegalArgumentException("Id cannot be null");
        }
        if (!isAuthenticatedUserAdmin()) {
            throw new IllegalArgumentException("User is not an admin");
        }
        User user = userRepository.findById(userId)
                .orElseThrow(() -> new ResourceNotFoundException("User with id " + userId + " not found"));
        if (!user.getRoles().contains(UserRole.ADMIN)) {
            user.getRoles().add(UserRole.ADMIN);
            userRepository.save(user);
        }
    }

    @Override
    public List<UserDTO> findAllAdmins() {
        return userMapper.usersToUserDtos(userRepository.findAllByRolesContains(UserRole.ADMIN));
    }

    @Override
    public Long getAuthenticatedUserId() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication instanceof BearerTokenAuthentication) {
            String userEmailFromToken = ((BearerTokenAuthentication) authentication).getTokenAttributes().get("sub").toString();
            var user = userRepository.findByEmail(userEmailFromToken);
            if (user.isPresent()) {
                return user.get().getId();
            } else {
                User savedUser = userRepository.save(new User(userEmailFromToken, new ArrayList<>(List.of(UserRole.USER))));
                return savedUser.getId();
            }
        } else {
            throw new IllegalArgumentException("Authentication is not BearerTokenAuthentication");
        }
    }

    @Override
    public Boolean isAuthenticatedUserAdmin() {
        Long userId = getAuthenticatedUserId();
        User user = userRepository.findById(userId)
                .orElseThrow(() -> new ResourceNotFoundException("User with id " + userId + " not found"));
        return user.getRoles().contains(UserRole.ADMIN);
    }


}
