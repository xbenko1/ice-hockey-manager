package cz.fi.muni.pa165.userservice.data;

import cz.fi.muni.pa165.enums.UserRole;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Schema(description = "Represents a User Entity.")
@Entity
@Table(name = "users")
public class User {

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Schema(description = "The ID of the user.", example = "1")
        private Long id;

        @NotBlank(message = "Email must not be blank")
        @Schema(description = "The email of the user.", example = "johndoemail.com")
        @Column(unique = true)
        private String email;
        @Schema(description = "The roles of the user.", example = "[\"USER\", \"ADMIN\"]")
        @ElementCollection
        @CollectionTable(name = "user_roles", joinColumns = @JoinColumn(name = "user_id"))
        @Column(name = "role")
        @Enumerated(EnumType.STRING)
        private List<UserRole> roles = new ArrayList<>(List.of(UserRole.USER));
        //Hello, create please insert sql script for this tamle based on this JPA entity:
        public User() {
        }

        public User(String email, List<UserRole> roles) {
                this.email = email;
                this.roles = roles;
        }

        public User(String email) {
                this.email = email;
        }

        public Long getId() {
                return id;
        }

        public void setId(Long id) {
                this.id = id;
        }

        public String getEmail() {
                return email;
        }

        public void setEmail(String email) {
                this.email = email;
        }

        public List<UserRole> getRoles() {
                return roles;
        }

        public void setRoles(List<UserRole> roles) {
                this.roles = roles;
        }
}
