package cz.fi.muni.pa165.userservice.rest;

import cz.fi.muni.pa165.dtos.*;
import cz.fi.muni.pa165.userservice.service.UserService;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.servers.Server;
import io.swagger.v3.oas.annotations.servers.ServerVariable;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@OpenAPIDefinition(
        info = @Info(title = "User API",
                version = "1.0",
                description = """
                        API for managing user in the ice hockey manager system
                        API defines following operations:
                        - get all users
                        - get user by id
                        - get user by username
                        - get user by email
                        - create new user
                        - update user
                        - delete user
                        - add team to user
                        - remove team from user
                        - update user to admin
                        - get all admins
                        """
        ),
        servers = @Server(description = "my server", url = "{scheme}://{server}:{port}", variables = {
                @ServerVariable(name = "scheme", allowableValues = {"http", "https"}, defaultValue = "http"),
                @ServerVariable(name = "server", defaultValue = "localhost"),
                @ServerVariable(name = "port", defaultValue = "8084"),
        }))

@Tag(name = "User", description = "tag.user.description")
@RequestMapping(path = "/api/users", produces = MediaType.APPLICATION_JSON_VALUE)
public class UserController {

    private final UserService userService;
    private static final Logger logger = LoggerFactory.getLogger(UserController.class);
    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }


    @Operation(summary = "operation.getAllUsers.summary",
            responses = @ApiResponse(responseCode = "200", description = "operation.getAllUsers.description",
            content = @Content(schema = @Schema(implementation = UserDTO.class)))
    )
    @ApiResponse(responseCode = "200", description = "operation.getAllUsers.description")
    @GetMapping
    public ResponseEntity<List<UserDTO>> getAllUsers() {
        logger.info("fetching all users");
        return ResponseEntity.ok(userService.getAllUsers());
    }

    @Operation(summary = "operation.getUserById.summary",
            responses = {
                    @ApiResponse(responseCode = "200", description = "operation.getUserById.found.description",
                            content = @Content(schema = @Schema(implementation = UserDTO.class))),
                    @ApiResponse(responseCode = "404", description = "operation.getUserById.notfound.description")
            })
    @ApiResponse(responseCode = "200", description = "operation.getUserById.found.description")
    @GetMapping(path = "/{id}")
    public ResponseEntity<UserDTO> getUserById(@PathVariable Long id) {
        logger.info("fetching user with id={}", id);
        return ResponseEntity.ok(userService.findUserById(id));
    }

    @Operation(summary = "operation.getUserByEmail.summary",
            responses = {
                    @ApiResponse(responseCode = "200", description = "operation.getUserByEmail.found.description",
                            content = @Content(schema = @Schema(implementation = UserDTO.class))),
                    @ApiResponse(responseCode = "404", description = "operation.getUserByEmail.notfound.description")
            }
    )
    @ApiResponse(responseCode = "200", description = "operation.getUserByEmail.found.description")
    @GetMapping(path = "/email/{email}")
    public ResponseEntity<UserDTO> getUserByEmail(@PathVariable String email) {
        logger.info("fetching user with email={}", email);
        return ResponseEntity.ok(userService.findUserByEmail(email));
    }

    @Operation(summary = "operation.deleteUser.summary",
            responses = @ApiResponse(responseCode = "204", description = "operation.deleteUser.deleted.description")
    )
    @ApiResponse(responseCode = "204", description = "operation.deleteUser.deleted.description")
    @DeleteMapping(path = "/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public ResponseEntity<Void> deleteUser(@PathVariable Long id) {
        logger.info("deleting user with id={}", id);
        userService.deleteUser(id);
        return ResponseEntity.noContent().build();
    }

    @Operation(summary = "operation.updateUserToAdmin.summary",
            responses = @ApiResponse(responseCode = "200", description = "operation.updateUserToAdmin.updated.description",
            content = @Content(schema = @Schema(implementation = UserDTO.class)))
    )
    @ApiResponse(responseCode = "200", description = "operation.updateUserToAdmin.updated.description")
    @PutMapping(path = "/{id}/admin")
    public ResponseEntity<UserDTO> updateUserToAdmin(@PathVariable Long id) {
        logger.info("updating user with id={} to admin", id);
        userService.updateUserToAdmin(id);
        return ResponseEntity.ok(userService.findUserById(id));
    }

    @Operation(summary = "operation.getAllAdmins.summary",
            responses = @ApiResponse(responseCode = "200", description = "operation.getAllAdmins.description",
            content = @Content(schema = @Schema(implementation = UserDTO.class)))
    )
    @ApiResponse(responseCode = "200", description = "operation.getAllAdmins.description")
    @GetMapping("/admins")
    public ResponseEntity<List<UserDTO>> getAllAdmins() {
        logger.info("fetching all admins");
        return ResponseEntity.ok(userService.findAllAdmins());
    }

    @Operation(summary = "operation.amIAdmin.summary",
            responses = @ApiResponse(responseCode = "200", description = "operation.amIAdmin.description")
    )
    @ApiResponse(responseCode = "200", description = "operation.amIAdmin.description")
    @GetMapping("/amIAdmin")
    public ResponseEntity<Boolean> amIAdmin() {
        logger.info("checking if authenticated user is admin");
        return ResponseEntity.ok(userService.isAuthenticatedUserAdmin());
    }

    @Operation(summary = "operation.getAuthenticatedUserId.summary",
            responses = @ApiResponse(responseCode = "200", description = "operation.getAuthenticatedUserId.description")
    )
    @ApiResponse(responseCode = "200", description = "operation.getAuthenticatedUserId.description")
    @GetMapping("/myId")
    public ResponseEntity<Long> getAuthenticatedUserId() {
        logger.info("creating user in DB if he doesn't exist, else return his id");
        return ResponseEntity.ok(userService.getAuthenticatedUserId());
    }

}
