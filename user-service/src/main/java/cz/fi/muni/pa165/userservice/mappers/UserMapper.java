package cz.fi.muni.pa165.userservice.mappers;

import cz.fi.muni.pa165.dtos.UserDTO;
import cz.fi.muni.pa165.userservice.data.User;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface UserMapper {

    UserDTO userToUserDto(User user);
    User userDtoToUser(UserDTO userDTO);

    List<User> userDtosToUsers (List<UserDTO> userDTOS);

    List<UserDTO> usersToUserDtos(List<User> users);
}
