package cz.fi.muni.pa165.userservice.repository;

import cz.fi.muni.pa165.userservice.data.User;
import cz.fi.muni.pa165.enums.UserRole;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findByEmail(String email);
    List<User> findAllByRolesContains(UserRole role);


}
