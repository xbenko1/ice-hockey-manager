package cz.fi.muni.pa165.userservice.service;

import cz.fi.muni.pa165.dtos.*;

import java.util.List;

public interface UserService {
    UserDTO findUserByEmail(String email);
    UserDTO findUserById(Long id);
    List<UserDTO> getAllUsers();
    void deleteUser(Long userId);
    void updateUserToAdmin(Long userId);
    List<UserDTO> findAllAdmins();
    Long getAuthenticatedUserId();
    Boolean isAuthenticatedUserAdmin();
}
