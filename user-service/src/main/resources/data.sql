-- Insert authors of project as users into the 'users' table
INSERT INTO users (email) VALUES
                                  ('514449@muni.cz'),
                                  ('553685@muni.cz'),
                                  ('485066@muni.cz'),
                                  ('485430@muni.cz');
-- Insert roles for users
INSERT INTO user_roles (user_id, role) VALUES
                                  (1, 'ADMIN'),
                                  (2, 'ADMIN'),
                                  (1, 'USER'),
                                  (2, 'USER'),
                                  (3, 'USER'),
                                  (4, 'USER');
