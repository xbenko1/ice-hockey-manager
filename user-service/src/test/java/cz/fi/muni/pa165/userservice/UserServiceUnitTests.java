package cz.fi.muni.pa165.userservice;

import cz.fi.muni.pa165.dtos.UserDTO;
import cz.fi.muni.pa165.userservice.data.User;
import cz.fi.muni.pa165.enums.UserRole;
import cz.fi.muni.pa165.userservice.mappers.UserMapper;
import cz.fi.muni.pa165.userservice.repository.UserRepository;
import cz.fi.muni.pa165.userservice.service.UserServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.times;

@ExtendWith(MockitoExtension.class)
class UserServiceUnitTests {

    @Mock
    private UserMapper userMapper;

    @Mock
    private UserRepository userRepository;

    private UserServiceImpl userService;

    private User user;
    private User adminUser;
    private UserDTO userDto;

    @BeforeEach
    void setUp() {
        userService = spy(new UserServiceImpl(userMapper, userRepository));

        user = new User("email@example.com");
        adminUser = new User("adminEmail@example.com", new ArrayList<>(Arrays.asList(UserRole.USER, UserRole.ADMIN)));
        userDto = new UserDTO();
        userDto.setId(1L);
        userDto.setRoles(List.of(UserRole.USER));


        lenient().when(userMapper.userToUserDto(any(User.class))).thenReturn(userDto);
        lenient().when(userMapper.usersToUserDtos(anyList())).thenReturn(List.of(userDto));
    }

    @Test
    void deleteUser_UserDeleted() {
        when(userRepository.findById(1L)).thenReturn(java.util.Optional.of(user));
        doNothing().when(userRepository).delete(user);
        doReturn(true).when(userService).isAuthenticatedUserAdmin();

        userService.deleteUser(1L);

        verify(userRepository, times(1)).delete(user);
        verify(userRepository, times(1)).findById(1L);
        verify(userRepository).delete(user);
    }

    @Test
    void getAllUsers_ShouldReturnUsers() {
        when(userRepository.findAll()).thenReturn(List.of(user));

        List<UserDTO> result = userService.getAllUsers();

        assertNotNull(result);
        assertEquals(1, result.size());

        verify(userMapper, times(1)).usersToUserDtos(anyList());
    }

    @Test
    void updateUserToAdmin_UserUpdated() {
        when(userRepository.findById(1L)).thenReturn(java.util.Optional.of(user));
        when(userRepository.save(any(User.class))).thenReturn(user);
        doReturn(true).when(userService).isAuthenticatedUserAdmin();

        userService.updateUserToAdmin(1L);

        verify(userRepository, times(1)).save(any(User.class));
        verify(userRepository, times(1)).findById(1L);
    }

    @Test
    void updateUserToAdmin_ShouldDoNothing_WhenUserIsAlreadyAdmin() {
        when(userRepository.findById(1L)).thenReturn(java.util.Optional.of(adminUser));
        doReturn(true).when(userService).isAuthenticatedUserAdmin();

        userService.updateUserToAdmin(1L);

        verify(userRepository, times(0)).save(any(User.class));
        verify(userRepository, times(1)).findById(1L);
    }

    @Test
    void findAllAdmins_ShouldReturnAdmins() {
        when(userRepository.findAllByRolesContains(UserRole.ADMIN)).thenReturn(List.of(adminUser));

        List<UserDTO> result = userService.findAllAdmins();

        assertNotNull(result);
        assertEquals(1, result.size());

        verify(userMapper, times(1)).usersToUserDtos(anyList());
    }

    @Test
    void findUserByEmail_ShouldReturnUser() {
        when(userRepository.findByEmail("email@example.com")).thenReturn(java.util.Optional.of(user));

        UserDTO result = userService.findUserByEmail("email@example.com");

        assertNotNull(result);
        assertEquals(1L, result.getId());
    }
}
