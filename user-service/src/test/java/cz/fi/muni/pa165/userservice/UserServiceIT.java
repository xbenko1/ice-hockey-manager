package cz.fi.muni.pa165.userservice;

//It is commented due to security usage in each service, we don't know what does auth server return and what should we mock

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static net.bytebuddy.matcher.ElementMatchers.is;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.jsonPath;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

//@SpringBootTest
//@AutoConfigureMockMvc
//class UserServiceIT {
//
//    @Autowired
//    private MockMvc mockMvc;
//
//    @Autowired
//    private ObjectMapper objectMapper;
//
//    @Test
//    void getAllUsers() throws Exception {
//        mockMvc.perform(get("/api/users"))
//                .andExpect(status().isOk());
//    }
//
//    @Test
//    void createUser() throws Exception {
//        // Create a new user
//        UserCreateDto newUser = new UserCreateDto("username", "email@example.com", "password");
//        String userJson = objectMapper.writeValueAsString(newUser);
//
//        String userLocation = mockMvc.perform(post("/api/users")
//                        .contentType(MediaType.APPLICATION_JSON)
//                        .content(userJson))
//                .andExpect(status().isCreated())
//                .andReturn().getResponse().getHeader("Location");
//
//        // Extract user ID from the location URL
//        assert userLocation != null;
//        String userId = userLocation.substring(userLocation.lastIndexOf('/') + 1);
//
//        // Get the user
//        mockMvc.perform(get("/api/users/{id}", userId))
//                .andExpect(status().isOk());
//
//        // Update the user
//        UserUpdateDto updatedUser = new UserUpdateDto("newUsername", "newEmail@example.com", "newPassword");
//        mockMvc.perform(put("/api/users/{id}", userId)
//                        .contentType(MediaType.APPLICATION_JSON)
//                        .content(objectMapper.writeValueAsString(newUser)))
//                .andExpect(status().isOk());
//
//        // Delete the user
//        mockMvc.perform(delete("/api/users/{id}", userId))
//                .andExpect(status().isNoContent());
//
//    }
//}
