package cz.fi.muni.pa165.player.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class PlayerServiceApp {

    private static final Logger logger = LoggerFactory.getLogger(PlayerServiceApp.class);

    // Retrieve server port and context-path from application.yml
    @Value("${server.port:8081}")
    private String serverPort;

    @Value("${springdoc.api-docs.path:/openapi}")
    private String apiDocsPath;

    @Value("${springdoc.swagger-ui.path:/swagger-ui.html}")
    private String swaggerUiPath;

    public static void main(String[] args) {
        ApplicationContext ctx = SpringApplication.run(PlayerServiceApp.class, args);

//        System.out.println("Let's inspect the beans provided by Spring Boot:");
//        String[] beanNames = ctx.getBeanDefinitionNames();
//        for (String beanName : beanNames) {
//            if (beanName.toLowerCase().contains("player")) {
//                System.out.println(beanName);
//            }
//        }
    }

    @Bean
    CommandLineRunner run() {
        return args -> {
            String baseUrl = "http://localhost:" + serverPort;
            logger.info("Player Service Application has started successfully.");
            logger.info("API Documentation: {}", baseUrl + apiDocsPath);
            logger.info("Swagger UI: {}", baseUrl + swaggerUiPath);
        };
    }
}
