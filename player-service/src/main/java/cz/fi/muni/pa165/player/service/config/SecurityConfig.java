package cz.fi.muni.pa165.player.service.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
@EnableWebSecurity
public class SecurityConfig {

    @Bean
    SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        http
                .authorizeHttpRequests(authorizeRequests ->
                        authorizeRequests
                        // for tests
//                        .requestMatchers(HttpMethod.GET, "/api/**").hasAuthority(Scope.TEST_READ.scopeName)
//                        .requestMatchers(HttpMethod.POST, "/api/**").hasAuthority(Scope.TEST_WRITE.scopeName)
//                        .requestMatchers(HttpMethod.PUT, "/api/**").hasAuthority(Scope.TEST_WRITE.scopeName)
//                        .requestMatchers(HttpMethod.DELETE, "/api/**").hasAuthority(Scope.TEST_WRITE.scopeName)
                        .anyRequest().authenticated()
                )
                .oauth2ResourceServer(oauth2 -> oauth2.opaqueToken(Customizer.withDefaults()));
        return http.build();
    }

}
