package cz.fi.muni.pa165.player.service.service;

import cz.fi.muni.pa165.player.service.repository.Player;
import cz.fi.muni.pa165.exceptions.PlayerDeleteException;
import cz.fi.muni.pa165.exceptions.PlayerNotFoundException;

import java.util.List;

public interface PlayerService {
    List<Player> getAllPlayers();
    List<Player> getAllFreeAgentPlayers();
    List<Player> getAllTeamPlayers(Long teamId) throws IllegalArgumentException;
    Player getPlayer(Long id) throws IllegalArgumentException, PlayerNotFoundException;
    Player createNewPlayer(Player player) throws IllegalArgumentException;
    Player updatePlayer(Long id,Player player) throws IllegalArgumentException, PlayerNotFoundException;
    Player addFreePlayerToTeam(Long playerId, Long teamId) throws IllegalArgumentException, PlayerNotFoundException;
    Player deletePlayer(Long id) throws IllegalArgumentException, PlayerNotFoundException, PlayerDeleteException;


}
