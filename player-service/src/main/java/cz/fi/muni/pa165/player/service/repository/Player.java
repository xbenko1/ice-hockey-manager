package cz.fi.muni.pa165.player.service.repository;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import io.swagger.v3.oas.annotations.media.Schema;

import java.time.LocalDate;


@Schema(description = "Represents a Hockey Player in the system.")
@Entity
@Table(name = "player")
public class Player {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Schema(description = "The ID of the player.", example = "1")
    private Long id;

    @Schema(description = "The team ID to which the player belongs.", example = "10")
    private Long teamId;

    @NotBlank(message = "First name must not be blank")
    @Schema(description = "The first name of the player.", example = "John")
    @Column(length = 50)
    private String firstName;

    @NotBlank(message = "Last name must not be blank")
    @Schema(description = "The last name of the player.", example = "Doe")
    @Column(length = 50)
    private String lastName;

    @NotNull(message = "Birth date must not be null")
    @Schema(description = "The birth date of the player.", example = "1996-01-01")
    private LocalDate birthDate;

    @NotNull(message = "Skill must not be null")
    @Positive(message = "Skill must be positive")
    @Schema(description = "The skill level of the player.", example = "80")
    private Integer skill;

    @NotNull(message = "Strength must not be null")
    @Positive(message = "Strength must be positive")
    @Schema(description = "The strength of the player.", example = "75")
    private Integer strength;

    @NotNull(message = "Stamina must not be null")
    @Positive(message = "Stamina must be positive")
    @Schema(description = "The stamina of the player.", example = "85")
    private Integer stamina;

    @NotNull(message = "Speed must not be null")
    @Positive(message = "Speed must be positive")
    @Schema(description = "The speed of the player.", example = "90")
    private Integer speed;

    @NotNull(message = "Height must not be null")
    @Positive(message = "Height must be positive")
    @Schema(description = "The height of the player in centimeters.", example = "180")
    private Integer height;

    @NotNull(message = "Weight must not be null")
    @Positive(message = "Weight must be positive")
    @Schema(description = "The weight of the player in kilograms.", example = "75")
    private Integer weight;

    @NotNull(message = "Price must not be null")
    @Positive(message = "Price must be positive")
    @Schema(description = "The market price of the player.", example = "1000000")
    private Integer price;

    public Player() {
    }

    public Player(Long id, Long teamId, String firstName, String lastName, LocalDate birthDate, Integer skill, Integer strength, Integer stamina, Integer speed, Integer height, Integer weight, Integer price) {
        this.id = id;
        this.teamId = teamId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthDate = birthDate;
        this.skill = skill;
        this.strength = strength;
        this.stamina = stamina;
        this.speed = speed;
        this.height = height;
        this.weight = weight;
        this.price = price;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getTeamId() {
        return teamId;
    }

    public void setTeamId(Long teamId) {
        this.teamId = teamId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public Integer getSkill() {
        return skill;
    }

    public void setSkill(Integer skill) {
        this.skill = skill;
    }

    public Integer getStrength() {
        return strength;
    }

    public void setStrength(Integer strength) {
        this.strength = strength;
    }

    public Integer getStamina() {
        return stamina;
    }

    public void setStamina(Integer stamina) {
        this.stamina = stamina;
    }

    public Integer getSpeed() {
        return speed;
    }

    public void setSpeed(Integer speed) {
        this.speed = speed;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }
}
