package cz.fi.muni.pa165.player.service.service;

import cz.fi.muni.pa165.dtos.TeamDTO;
import cz.fi.muni.pa165.player.service.repository.Player;
import cz.fi.muni.pa165.player.service.repository.PlayerRepository;
import cz.fi.muni.pa165.exceptions.PlayerDeleteException;
import cz.fi.muni.pa165.exceptions.PlayerNotFoundException;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.server.resource.authentication.BearerTokenAuthentication;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.Optional;

@Service
public class PlayerServiceImpl implements PlayerService {

    @Value("${team-service.url}")
    private String teamServiceUrl;
    static public String ID_NOT_NULL_ERROR = "id can't be null";

    private final PlayerRepository playerRepository;

    private final WebClient webClient;

    @Autowired
    public PlayerServiceImpl(PlayerRepository playerRepository, WebClient webClient) {
        this.playerRepository = playerRepository;
        this.webClient = webClient;
    }

    @Transactional
    @Override
    public List<Player> getAllPlayers() {
        return playerRepository.findAll();
    }

    @Transactional
    @Override
    public List<Player> getAllFreeAgentPlayers() {
        return playerRepository.findByTeamIdIsNull();
    }

    @Transactional
    @Override
    public List<Player> getAllTeamPlayers(Long teamId) throws IllegalArgumentException {
        if (teamId == null) {
            throw new IllegalArgumentException("teamId can't be null");
        }
        return playerRepository.findByTeamId(teamId);
    }

    @Transactional
    @Override
    public Player getPlayer(Long id) throws IllegalArgumentException, PlayerNotFoundException {
        if (id == null) {
            throw new IllegalArgumentException(ID_NOT_NULL_ERROR);
        }
        // if not present in the optional throw exception
        return playerRepository.findById(id).orElseThrow(() -> new PlayerNotFoundException("Player with id " + id + " not found."));
    }

    @Transactional
    @Override
    public Player createNewPlayer(Player player) throws IllegalArgumentException {
        if (!amIAdmin()) {
            throw new IllegalArgumentException("Only admin can create a player");
        }
        if (player == null) {
            throw new IllegalArgumentException("Dto can't be null");
        }
        if (player.getTeamId() != null) {
            if (!teamExists(player.getTeamId())) {
                throw new IllegalArgumentException("Team with id " + player.getTeamId() + " not found.");
            }

        }
        return playerRepository.save(player);

    }

    @Transactional
    @Override
    public Player updatePlayer(Long id, Player player) throws IllegalArgumentException, PlayerNotFoundException {
        if (!amIAdmin()) {
            throw new IllegalArgumentException("Only admin can update a player");
        }
        if (id == null) {
            throw new IllegalArgumentException(ID_NOT_NULL_ERROR);
        }
        if (player == null) {
            throw new IllegalArgumentException("player can't be null");
        }
        if (!playerRepository.existsById(id)) {
            throw new PlayerNotFoundException("Player with id " + id + " not found.");
        }
        player.setId(id);
        if (player.getTeamId() != null) {
            if (!teamExists(player.getTeamId())) {
                throw new IllegalArgumentException("Team with id " + player.getTeamId() + " not found.");
            }
        }
        return playerRepository.save(player);
    }

    @Transactional
    @Override
    public Player addFreePlayerToTeam(Long playerId, Long teamId) throws IllegalArgumentException, PlayerNotFoundException {
        if (playerId == null || teamId == null) {
            throw new IllegalArgumentException("playerId or teamId can't be null");
        }
        Optional<Player> player = playerRepository.findById(playerId);
        if (player.isEmpty()) {
            throw new PlayerNotFoundException("Player with id " + playerId + " not found.");
        }
        if (!canAddToTeam(teamId, myId())) {
            throw new IllegalArgumentException("Team with id " + teamId + " not found or you are not the manager of the team.");
        }
        player.get().setTeamId(teamId);
        return playerRepository.save(player.get());
    }


    @Transactional
    @Override
    public Player deletePlayer(Long id) throws IllegalArgumentException, PlayerNotFoundException, PlayerDeleteException {
        if (!amIAdmin()) {
            throw new IllegalArgumentException("Only admin can delete a player");
        }
        if (id == null) {
            throw new IllegalArgumentException(ID_NOT_NULL_ERROR);
        }
        Player playerToDelete = getPlayer(id);
        if (playerToDelete.getTeamId() != null) {
            throw new PlayerDeleteException();
        }
        playerRepository.delete(playerToDelete);
        return playerToDelete;
    }

    public boolean amIAdmin() {
        String jwt = extractJwtToken();
        Mono<Boolean> isAdmin = webClient.get()
                .uri("api/users/amIAdmin")
                .header("Authorization", "Bearer " + jwt)
                .retrieve()
                .bodyToMono(Boolean.class);
        return Boolean.TRUE.equals(isAdmin.block());
    }

    public Long myId() {
        String jwt = extractJwtToken();
        Mono<Long> myId = webClient.get()
                .uri("api/users/myId")
                .header("Authorization", "Bearer " + jwt)
                .retrieve()
                .bodyToMono(Long.class);
        return myId.block();
    }

    public boolean teamExists(Long teamId) {
        String jwt = extractJwtToken();
        HttpStatus status = (HttpStatus) webClient.mutate()
                .baseUrl(teamServiceUrl).build().get()
                .uri("/{id}", teamId)
                .header("Authorization", "Bearer " + jwt)
                .retrieve()
                .toBodilessEntity()
                .map(ResponseEntity::getStatusCode)
                .onErrorReturn(HttpStatus.NOT_FOUND)
                .block();

        if (status == null) {
            return false;
        }
        return status.equals(HttpStatus.OK);
    }


    private boolean canAddToTeam(Long teamId, Long myId) {
        // extract team like in exist team but get the dto
        String jwt = extractJwtToken();
        Mono<TeamDTO> team = webClient.mutate()
                .baseUrl(teamServiceUrl).build().get()
                .uri("/{id}", teamId)
                .header("Authorization", "Bearer " + jwt)
                .retrieve()
                .bodyToMono(TeamDTO.class);
        // check if the team exists
        if (team.block() == null) {
            return false;
        }
        // check if I am the manager
        if (!team.block().getManagerId().equals(myId)) {
            return false;
        }
        return true;

    }


    private String extractJwtToken() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        try {
            if (authentication instanceof BearerTokenAuthentication) {
                BearerTokenAuthentication jwtToken = (BearerTokenAuthentication) authentication;
                return jwtToken.getToken().getTokenValue();
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return "";
    }


}
