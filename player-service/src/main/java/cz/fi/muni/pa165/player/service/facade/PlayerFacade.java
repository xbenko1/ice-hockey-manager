package cz.fi.muni.pa165.player.service.facade;

import cz.fi.muni.pa165.dtos.BasePlayerDTO;
import cz.fi.muni.pa165.dtos.PlayerDTO;
import cz.fi.muni.pa165.exceptions.PlayerDeleteException;
import cz.fi.muni.pa165.exceptions.PlayerNotFoundException;
import cz.fi.muni.pa165.player.service.mappers.PlayerMapper;
import cz.fi.muni.pa165.player.service.repository.Player;
import cz.fi.muni.pa165.player.service.service.PlayerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class PlayerFacade {

    private final PlayerService playerService;
    private final PlayerMapper playerMapper;

    @Autowired
    public PlayerFacade(PlayerService playerService, PlayerMapper playerMapper) {
        this.playerService = playerService;
        this.playerMapper = playerMapper;
    }

    public List<PlayerDTO> getAllPlayers() {
        return playerService.getAllPlayers().stream()
                .map(playerMapper::toDTO)
                .collect(Collectors.toList());
    }

    public List<PlayerDTO> getAllFreeAgentPlayers() {
        return playerService.getAllFreeAgentPlayers().stream()
                .map(playerMapper::toDTO)
                .collect(Collectors.toList());
    }

    public List<PlayerDTO> getAllTeamPlayers(Long teamId) {
        return playerService.getAllTeamPlayers(teamId).stream()
                .map(playerMapper::toDTO)
                .collect(Collectors.toList());
    }

    public PlayerDTO getPlayer(Long id) throws PlayerNotFoundException {
        Player player = playerService.getPlayer(id);
        return playerMapper.toDTO(player);
    }

    public PlayerDTO createNewPlayer(BasePlayerDTO basePlayerDTO) {
        Player newPlayer = playerService.createNewPlayer(playerMapper.toEntity(basePlayerDTO));
        return playerMapper.toDTO(newPlayer);
    }
    public PlayerDTO addFreePlayerToTeam(Long playerId, Long teamId) throws PlayerNotFoundException,IllegalArgumentException {
        Player player = playerService.addFreePlayerToTeam(playerId, teamId);
        return playerMapper.toDTO(player);
    }

    public PlayerDTO updatePlayer(Long id, BasePlayerDTO basePlayerDTO) throws PlayerNotFoundException {
        Player updatedPlayer = playerService.updatePlayer(id, playerMapper.toEntity(basePlayerDTO));
        return playerMapper.toDTO(updatedPlayer);
    }

    public PlayerDTO deletePlayer(Long id) throws PlayerNotFoundException, PlayerDeleteException {
        Player deletedPlayer = playerService.deletePlayer(id);
        return playerMapper.toDTO(deletedPlayer);
    }
}