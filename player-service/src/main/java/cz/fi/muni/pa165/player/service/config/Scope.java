package cz.fi.muni.pa165.player.service.config;

public enum Scope {

    TEST_WRITE("test_write"),
    TEST_READ("test_read");

    public final String scopeName;

    private Scope(String label) {
        this.scopeName = label;
    }
}
