package cz.fi.muni.pa165.player.service.mappers;

import cz.fi.muni.pa165.dtos.BasePlayerDTO;
import cz.fi.muni.pa165.dtos.PlayerDTO;
import cz.fi.muni.pa165.player.service.repository.Player;
import org.mapstruct.Mapper;


@Mapper(componentModel = "spring")
public interface PlayerMapper {


    PlayerDTO toDTO(Player player);

    Player toEntity(PlayerDTO dto);
    Player toEntity(BasePlayerDTO baseDto);

}