package cz.fi.muni.pa165.player.service.rest;

import cz.fi.muni.pa165.dtos.BasePlayerDTO;
import cz.fi.muni.pa165.dtos.PlayerDTO;
import cz.fi.muni.pa165.exceptions.PlayerDeleteException;
import cz.fi.muni.pa165.exceptions.PlayerNotFoundException;
import cz.fi.muni.pa165.player.service.facade.PlayerFacade;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.servers.Server;
import io.swagger.v3.oas.annotations.servers.ServerVariable;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;


@OpenAPIDefinition(
        info = @Info(title = "Player API",
                version = "1.0",
                description = """
                        API for managing players in the ice hockey manager system
                        API defines following operations:
                        - getting all players
                        - get free agent players
                        - get all players of a team
                        - get player by id
                        - create new player
                        - update player
                        - delete player
                        """
        ),
        servers = @Server(description = "my server", url = "{scheme}://{server}:{port}", variables = {
                @ServerVariable(name = "scheme", allowableValues = {"http", "https"}, defaultValue = "http"),
                @ServerVariable(name = "server", defaultValue = "localhost"),
                @ServerVariable(name = "port", defaultValue = "8081"),
        }))

@Tag(name = "Player", description = "tag.player.description")
@RequestMapping(path = "/api", produces = MediaType.APPLICATION_JSON_VALUE)
@RestController
public class PlayerController {

    private static final Logger logger = LoggerFactory.getLogger(PlayerController.class);
    private final PlayerFacade playerFacade;

    @Autowired
    public PlayerController(PlayerFacade playerFacade) {
        this.playerFacade = playerFacade;
    }

    @Operation(summary = "operation.getAllPlayers.summary",
            responses = @ApiResponse(responseCode = "200", description = "operation.getAllPlayers.description",
            content = @Content(schema = @Schema(implementation = PlayerDTO.class))))
    @GetMapping("/players")
    public ResponseEntity<List<PlayerDTO>> getAllPlayers() {
        logger.info("Fetching all players");
        return ResponseEntity.ok(playerFacade.getAllPlayers());
    }

    @Operation(summary = "operation.getAllFreeAgentPlayers.summary",
            responses = @ApiResponse(responseCode = "200", description = "operation.getAllFreeAgentPlayers.description",
            content = @Content(schema = @Schema(implementation = PlayerDTO.class))))
    @GetMapping("/players/free-agents")
    public ResponseEntity<List<PlayerDTO>> getAllFreeAgentPlayers() {
        logger.info("Fetching all free agent players");
        return ResponseEntity.ok(playerFacade.getAllFreeAgentPlayers());
    }

    @Operation(summary = "operation.getAllTeamPlayers.summary",
            responses =
            @ApiResponse(responseCode = "200", description = "operation.getAllTeamPlayers.description",
                        content = @Content(schema = @Schema(implementation = PlayerDTO.class)))
            )
    @GetMapping("/players/team/{teamId}")
    public ResponseEntity<List<PlayerDTO>> getAllTeamPlayers(@PathVariable Long teamId) {
        logger.info("Fetching all players of team with id={}",teamId);
        return ResponseEntity.ok(playerFacade.getAllTeamPlayers(teamId));
    }

    @Operation(summary = "operation.getPlayer.summary",
            responses = {
                    @ApiResponse(responseCode = "200", description = "operation.getPlayer.found.description",
                            content = @Content(schema = @Schema(implementation = PlayerDTO.class))),
                    @ApiResponse(responseCode = "404", description = "operation.getPlayer.notfound.description")
            })
    @GetMapping("/players/{id}")
    public ResponseEntity<PlayerDTO> getPlayer(@PathVariable Long id) {
        try {
            logger.info("Fetching player with id={}",id);
            return ResponseEntity.ok(playerFacade.getPlayer(id));
        } catch (PlayerNotFoundException e) {
            logger.warn("Player not found with id={}",id);
            return ResponseEntity.notFound().build();
        }
    }

    @Operation(summary = "operation.createNewPlayer.summary",
            responses = @ApiResponse(responseCode = "201", description = "operation.createNewPlayer.description",
            content = @Content(schema = @Schema(implementation = PlayerDTO.class))))
    @PostMapping("/players")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<?> createNewPlayer(@Valid @RequestBody BasePlayerDTO basePlayerDTO) {
        logger.info("Creating new player with name {} {} ",basePlayerDTO.getFirstName(),basePlayerDTO.getLastName());
        try {
            PlayerDTO newPlayer = playerFacade.createNewPlayer(basePlayerDTO);
            URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
                    .buildAndExpand(newPlayer.getId()).toUri();
            return ResponseEntity.created(location).body(newPlayer);
        } catch (IllegalArgumentException e) {
            logger.error("Error creating player: {}", e.getMessage());
            return ResponseEntity.badRequest().body(new ErrorMessage(e.getMessage()));
        }

    }

    @Operation(summary = "operation.updatePlayer.summary",
            responses = {
                    @ApiResponse(responseCode = "200", description = "operation.updatePlayer.updated.description",
                            content = @Content(schema = @Schema(implementation = PlayerDTO.class))),
                    @ApiResponse(responseCode = "400", description = "operation.updatePlayer.validation.description"),
                    @ApiResponse(responseCode = "404", description = "operation.updatePlayer.notfound.description")
            })
    @PutMapping("/player/{id}")
    public ResponseEntity<?> updatePlayer(@PathVariable Long id,
                                          @Valid @RequestBody BasePlayerDTO playerDTO,
                                          BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            String errorMessages = bindingResult.getFieldErrors().stream()
                    .map(fe -> fe.getField() + " - " + fe.getDefaultMessage())
                    .collect(Collectors.joining(", "));
            logger.error("Validation errors while updating player {}: {}", id, errorMessages);
            return ResponseEntity.badRequest().body(new ErrorMessage(errorMessages));
        }

        try {
            logger.info("Updating player with ID: {}", id);
            return ResponseEntity.ok(playerFacade.updatePlayer(id, playerDTO));
        } catch (IllegalArgumentException e) {
            logger.error("Error updating player {}: {}", id, e.getMessage());
            return ResponseEntity.badRequest().body(new ErrorMessage(e.getMessage()));
        } catch (PlayerNotFoundException e) {
            logger.warn("Player not found with id={}",id);
            return ResponseEntity.notFound().build();
        }
    }

    @Operation(summary = "operation.addFreePlayerToTeam.summary",
            responses = {
                    @ApiResponse(responseCode = "200", description = "operation.addFreePlayerToTeam.added.description",
                            content = @Content(schema = @Schema(implementation = PlayerDTO.class))),
                    @ApiResponse(responseCode = "404", description = "operation.addFreePlayerToTeam.notfound.description")
            })
    @PostMapping("/players/{playerId}/team/{teamId}")
    public ResponseEntity<?> addFreePlayerToTeam(@PathVariable Long playerId, @PathVariable Long teamId) {
        try {
            logger.info("Adding player with ID: {} to team with ID: {}", playerId, teamId);
            return ResponseEntity.ok(playerFacade.addFreePlayerToTeam(playerId, teamId));
        } catch (IllegalArgumentException e) {
            logger.error("Error adding player to team: {}", e.getMessage());
            return ResponseEntity.badRequest().body(new ErrorMessage(e.getMessage()));
        } catch (PlayerNotFoundException e) {
            logger.warn("Player not found with id={}", playerId);
            return ResponseEntity.notFound().build();
        }
    }

    @Operation(summary = "operation.deletePlayer.summary",
            responses = {
                    @ApiResponse(responseCode = "200", description = "operation.deletePlayer.deleted.description",
                            content = @Content(schema = @Schema(implementation = PlayerDTO.class))),
                    @ApiResponse(responseCode = "404", description = "operation.deletePlayer.notfound.description"),
                    @ApiResponse(responseCode = "409", description = "operation.deletePlayer.conflict.description")
            })
    @DeleteMapping("/player/{id}")
    public ResponseEntity<?> deletePlayer(@PathVariable Long id) {
        try {
            logger.info("Deleting player with ID: {}", id);
            return ResponseEntity.ok(playerFacade.deletePlayer(id));
        } catch (PlayerNotFoundException e) {
            logger.error("Player not found with id={}",id);
            return ResponseEntity.notFound().build();
        } catch (PlayerDeleteException e) {
            logger.warn("Tried to delete player in team.");
            return ResponseEntity
                    .status(HttpStatus.CONFLICT)
                    .body(new ErrorMessage("Cannot delete player that is in a team."));
        }
    }

    public static class ErrorMessage {
        private String message;

        public ErrorMessage(String message) {
            this.message = message;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }


}