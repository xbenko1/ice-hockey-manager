package cz.fi.muni.pa165.player.service.repository;


import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PlayerRepository extends JpaRepository<Player, Long> {

    List<Player> findByLastName(String lastName);
    List<Player> findByTeamId(Long teamId);
    List<Player> findByTeamIdIsNull();
}