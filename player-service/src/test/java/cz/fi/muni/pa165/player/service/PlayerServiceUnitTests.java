package cz.fi.muni.pa165.player.service;


import cz.fi.muni.pa165.dtos.BasePlayerDTO;
import cz.fi.muni.pa165.player.service.repository.Player;
import cz.fi.muni.pa165.player.service.repository.PlayerRepository;
import cz.fi.muni.pa165.player.service.service.PlayerServiceImpl;
import cz.fi.muni.pa165.exceptions.PlayerDeleteException;
import cz.fi.muni.pa165.exceptions.PlayerNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.web.reactive.function.client.WebClient;

import java.time.LocalDate;
import java.util.Optional;

import static org.hibernate.validator.internal.util.Contracts.assertNotNull;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;


@SpringBootTest
public class PlayerServiceUnitTests {

    @Mock
    private PlayerRepository playerRepository;
    @Mock
    private WebClient webClient;

    private PlayerServiceImpl playerService;

    private final BasePlayerDTO freeAgentBaseDto =
            new BasePlayerDTO(null, "John", "Doe",
                    LocalDate.of(2000,1,1), 80, 75, 85, 90, 180, 75, 1000000);

    private final Player teamPlayerOne =
            new Player(1L,1L, "John2", "Doe2",
                    LocalDate.of(2000,1,1), 80, 75, 85, 90, 180, 75, 1000000);

    private final Player teamPlayerTwo =
            new Player(2L,2L, "John3", "Doe3",
                    LocalDate.of(2000,1,1), 80, 75, 85, 90, 180, 75, 1000000);

    @BeforeEach
    void setUp() {
        playerService = spy(new PlayerServiceImpl(playerRepository, webClient));
        doReturn(true).when(playerService).amIAdmin();
    }


    @Test
    void createNewPlayer_Success() {
        Player expectedPlayer = new Player();
        expectedPlayer.setFirstName("New");

        // Repository mock
        when(playerRepository.save(any(Player.class))).thenReturn(expectedPlayer);

        Player created = playerService.createNewPlayer(expectedPlayer);
        assertNotNull(created);
        assertEquals("New", created.getFirstName());
        verify(playerRepository, times(1)).save(any(Player.class));
    }
    @Test
    void createNewPlayerInTeam_Success() {
        Player expectedPlayer = new Player();
        expectedPlayer.setFirstName("New");
        expectedPlayer.setTeamId(1L);

        doReturn(true).when(playerService).teamExists(1L);

        // Repository mock
        when(playerRepository.save(any(Player.class))).thenReturn(expectedPlayer);

        Player created = playerService.createNewPlayer(expectedPlayer);
        assertNotNull(created);
        assertEquals("New", created.getFirstName());
        verify(playerRepository, times(1)).save(any(Player.class));
    }

    @Test
    void createNewPlayer_Fail_NullDTO() {
        assertThrows(IllegalArgumentException.class, () -> playerService.createNewPlayer(null));
        verify(playerRepository, times(0)).save(any(Player.class));
    }
    @Test
    void createNewPlayer_Fail_NotAdmin() {
        doReturn(false).when(playerService).amIAdmin();
        assertThrows(IllegalArgumentException.class, () -> playerService.createNewPlayer(teamPlayerOne));
        verify(playerRepository, times(0)).save(any(Player.class));
    }

    @Test
    void getPlayer_Success() {
        Player expectedPlayer = new Player();
        expectedPlayer.setId(1L);
        when(playerRepository.findById(1L)).thenReturn(Optional.of(expectedPlayer));

        Player actualPlayer = assertDoesNotThrow(() -> playerService.getPlayer(1L));
        assertNotNull(actualPlayer);
        assertEquals(expectedPlayer.getId(), actualPlayer.getId());
    }


    @Test
    void getPlayer_NullId_ThrowsIllegalArgumentException() {
        IllegalArgumentException thrown = assertThrows(
                IllegalArgumentException.class,
                () -> playerService.getPlayer(null),
                "Expected getPlayer(null) to throw, but it didn't"
        );

        assertTrue(thrown.getMessage().contains(PlayerServiceImpl.ID_NOT_NULL_ERROR));
    }

    @Test
    void getPlayer_NonExistingId_ThrowsPlayerNotFoundException() {
        Long nonExistingId = Long.MAX_VALUE;
        when(playerRepository.findById(nonExistingId)).thenReturn(Optional.empty());

        assertThrows(
                PlayerNotFoundException.class,
                () -> playerService.getPlayer(nonExistingId),
                "Expected getPlayer(nonExistingId) to throw PlayerNotFoundException, but it didn't"
        );
    }

    @Test
    void updatePlayer_Success() throws PlayerNotFoundException {
        Player expectedPlayer = new Player();
        expectedPlayer.setFirstName("Updated");

        when(playerRepository.existsById(1L)).thenReturn(true);
        when(playerRepository.save(any(Player.class))).thenReturn(expectedPlayer);

        Player updated = playerService.updatePlayer(1L, expectedPlayer);

        assertNotNull(updated);
        assertEquals("Updated", updated.getFirstName());
        verify(playerRepository, times(1)).save(any(Player.class));
    }

    @Test
    void updatePlayer_Fail_NotFound() {
        Long nonExistingId = 99L;
        when(playerRepository.findById(nonExistingId)).thenReturn(Optional.empty());

        assertThrows(PlayerNotFoundException.class, () -> playerService.updatePlayer(nonExistingId, teamPlayerOne));
    }

    @Test
    void updatePlayer_Fail_IllegalId() {
        assertThrows(IllegalArgumentException.class, () -> playerService.updatePlayer(null, teamPlayerOne));
        verify(playerRepository, times(0)).findById(any());
        verify(playerRepository, times(0)).save(any(Player.class));
    }

    @Test
    void updatePlayer_Fail_IllegalDTO() {
        Long existingId = 1L;
        Player existingPlayer = new Player();
        existingPlayer.setId(existingId);
        when(playerRepository.findById(existingId)).thenReturn(Optional.of(existingPlayer));

        assertThrows(IllegalArgumentException.class, () -> playerService.updatePlayer(existingId, null));
        verify(playerRepository, times(0)).save(any(Player.class));
    }

    @Test
    void deletePlayer_Success() throws PlayerDeleteException, PlayerNotFoundException {
        Long existingId = 1L;
        Player existingPlayer = new Player();
        existingPlayer.setId(existingId);
        existingPlayer.setTeamId(null); // Assuming the player is not in a team
        when(playerRepository.findById(existingId)).thenReturn(Optional.of(existingPlayer));

         playerService.deletePlayer(existingId);

        verify(playerRepository, times(1)).delete(existingPlayer);
    }

    @Test
    void deletePlayer_Fail_PlayerInTeam() {
        Long existingId = 2L;
        Player existingPlayer = new Player();
        existingPlayer.setId(existingId);
        existingPlayer.setTeamId(1L); // Assuming the player is in a team
        when(playerRepository.findById(existingId)).thenReturn(Optional.of(existingPlayer));

        assertThrows(PlayerDeleteException.class, () -> playerService.deletePlayer(existingId));
        verify(playerRepository, times(0)).deleteById(existingId);
    }

    @Test
    void deletePlayer_Fail_NotFound() {
        Long nonExistingId = 99L;
        when(playerRepository.findById(nonExistingId)).thenReturn(Optional.empty());

        assertThrows(PlayerNotFoundException.class, () -> playerService.deletePlayer(nonExistingId));
        verify(playerRepository, times(0)).deleteById(nonExistingId);
    }

}
