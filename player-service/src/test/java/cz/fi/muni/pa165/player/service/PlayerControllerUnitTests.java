//package cz.fi.muni.pa165.player.service;
//
//import com.fasterxml.jackson.databind.ObjectMapper;
//import cz.fi.muni.pa165.dtos.BasePlayerDTO;
//import cz.fi.muni.pa165.dtos.PlayerDTO;
//import cz.fi.muni.pa165.exceptions.PlayerDeleteException;
//import cz.fi.muni.pa165.exceptions.PlayerNotFoundException;
//import cz.fi.muni.pa165.player.service.facade.PlayerFacade;
//import cz.fi.muni.pa165.player.service.mappers.PlayerMapper;
//import cz.fi.muni.pa165.player.service.rest.PlayerController;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
//import org.springframework.boot.test.mock.mockito.MockBean;
//import org.springframework.http.MediaType;
//import org.springframework.security.test.context.support.WithMockUser;
//import org.springframework.test.context.ActiveProfiles;
//import org.springframework.test.web.servlet.MockMvc;
//
//import java.time.LocalDate;
//import java.util.List;
//
//import static org.hamcrest.Matchers.hasSize;
//import static org.hamcrest.Matchers.is;
//import static org.mockito.ArgumentMatchers.any;
//import static org.mockito.ArgumentMatchers.eq;
//import static org.mockito.BDDMockito.given;
//import static org.mockito.Mockito.*;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
//
//@WebMvcTest(PlayerController.class)
//@ActiveProfiles("test")
//class PlayerControllerUnitTests {
//
//    @Autowired
//    private MockMvc mockMvc;
//
//    @MockBean
//    private PlayerFacade playerFacade;
//    @Autowired
//    private ObjectMapper objectMapper;
//
//    PlayerDTO playerOne = new PlayerDTO(
//            1L, // id
//            100L, // teamId
//            "John", // firstName
//            "Doe", // lastName
//            LocalDate.of(2000,1,1), // birthDate
//            80, // skill
//            75, // strength
//            85, // stamina
//            90, // speed
//            180, // height in centimeters
//            75, // weight in kilograms
//            1000000 // price
//    );
//    BasePlayerDTO playerOneBaseDTO = new BasePlayerDTO(
//            100L, // teamId
//            "John", // firstName
//            "Doe", // lastName
//            LocalDate.of(2000,1,1), // birthDate
//            80, // skill
//            75, // strength
//            85, // stamina
//            90, // speed
//            180, // height in centimeters
//            75, // weight in kilograms
//            1000000 // price
//    );
//
//    PlayerDTO playerTwo = new PlayerDTO(
//            2L, // id
//            null, // teamId, indicating this player is a free agent
//            "Jane", // firstName
//            "Roe", // lastName
//            LocalDate.of(2000,1,1), // birthDate
//            78, // skill
//            70, // strength
//            88, // stamina
//            92, // speed
//            175, // height in centimeters
//            70, // weight in kilograms
//            1200000 // price
//    );
//
//    BasePlayerDTO playerTwoBaseDTO = new BasePlayerDTO(
//            null, // teamId, indicating this player is a free agent
//            "Jane", // firstName
//            "Roe", // lastName
//            LocalDate.of(2000,1,1), // birthDate
//            78, // skill
//            70, // strength
//            88, // stamina
//            92, // speed
//            175, // height in centimeters
//            70, // weight in kilograms
//            1200000 // price
//    );
//
//
//    List<BasePlayerDTO> invalidDTOs = List.of(
//            new BasePlayerDTO(1L, "", "Doe", LocalDate.of(2000,1,1), 80, 75, 85, 90, 180, 75, 1000000), // Invalid firstName
//            new BasePlayerDTO(1L, "John", "", LocalDate.of(2000,1,1), 80, 75, 85, 90, 180, 75, 1000000), // Invalid lastName
//            new BasePlayerDTO(1L, "John", "Doe", LocalDate.of(2000,1,1), -1, 75, 85, 90, 180, 75, 1000000), // Invalid skill
//            new BasePlayerDTO(1L, "John", "Doe", LocalDate.of(2000,1,1), 80, -1, 85, 90, 180, 75, 1000000), // Invalid strength
//            new BasePlayerDTO(1L, "John", "Doe", LocalDate.of(2000,1,1), 80, 75, -1, 90, 180, 75, 1000000), // Invalid stamina
//            new BasePlayerDTO(1L, "John", "Doe", LocalDate.of(2000,1,1), 80, 75, 85, -1, 180, 75, 1000000), // Invalid speed
//            new BasePlayerDTO(1L, "John", "Doe", LocalDate.of(2000,1,1), 80, 75, 85, 90, -1, 75, 1000000), // Invalid height
//            new BasePlayerDTO(1L, "John", "Doe", LocalDate.of(2000,1,1), 80, 75, 85, 90, 180, -1, 1000000), // Invalid weight
//            new BasePlayerDTO(1L, "John", "Doe", LocalDate.of(2000,1,1), 80, 75, 85, 90, 180, 75, -1) // Invalid price
//    );
//
//
//    @Test
//    void getAllPlayers_Success() throws Exception {
//        given(playerFacade.getAllPlayers()).willReturn(List.of(playerOne, playerTwo));
//
//        mockMvc.perform(get("/api/players"))
//                .andExpect(status().isOk())
//                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
//                .andExpect(jsonPath("$", hasSize(2)))
//                .andExpect(jsonPath("$[0].id", is(1)))
//                .andExpect(jsonPath("$[1].id", is(2)));
//    }
//
//    @Test
//    @WithMockUser(authorities = "SCOPE_TEST_READ")
//    void getAllFreeAgents_Success() throws Exception {
//        given(playerFacade.getAllFreeAgentPlayers()).willReturn(List.of(playerTwo));
//
//        mockMvc.perform(get("/api/players/free-agents"))
//                .andExpect(status().isOk())
//                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
//                .andExpect(jsonPath("$", hasSize(1)))
//                .andExpect(jsonPath("$[0].id", is(2)));
//    }
//
//    @Test
//    @WithMockUser(authorities = "SCOPE_TEST_READ")
//    void getAllTeamPlayers_Success() throws Exception {
//        Long teamId = 100L;
//        given(playerFacade.getAllTeamPlayers(teamId)).willReturn(List.of(playerOne));
//
//        mockMvc.perform(get("/api/players/team/{teamId}", teamId))
//                .andExpect(status().isOk())
//                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
//                .andExpect(jsonPath("$", hasSize(1)))
//                .andExpect(jsonPath("$[0].teamId", is(teamId.intValue())));
//    }
//
//    @Test
//    @WithMockUser(authorities = "SCOPE_TEST_READ")
//    void getPlayer_Success() throws Exception, PlayerNotFoundException {
//        Long playerId = 1L;
//
//        given(playerFacade.getPlayer(playerId)).willReturn(playerOne);
//
//        mockMvc.perform(get("/api/players/{id}", playerId))
//                .andExpect(status().isOk())
//                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
//                .andExpect(jsonPath("$.id", is(playerId.intValue())));
//    }
//
//    @Test
//    @WithMockUser(authorities = "test_read")
//    void getPlayer_Fail_NotFound() throws Exception, PlayerNotFoundException {
//        Long playerId = 99L; // Assuming this ID doesn't exist
//        given(playerFacade.getPlayer(playerId)).willThrow(new PlayerNotFoundException("Player with id: " + playerId + " was not found."));
//
//        mockMvc.perform(get("/api/players/{id}", playerId))
//                .andExpect(status().isNotFound());
//    }
//
//    @Test
//    @WithMockUser(authorities = "test_write")
//    void createNewPlayer_Success() throws Exception {
//        when(playerFacade.createNewPlayer(any(BasePlayerDTO.class))).thenReturn(playerOne);
//
//        mockMvc.perform(post("/api/players")
//                        .contentType(MediaType.APPLICATION_JSON)
//                        .content(objectMapper.writeValueAsString(playerOneBaseDTO)))
//                .andExpect(status().isCreated())
//                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
//                .andExpect(jsonPath("$.id", is(1)))
//                .andExpect(jsonPath("$.firstName", is("John")))
//                .andExpect(jsonPath("$.lastName", is("Doe")))
//                .andExpect(jsonPath("$.birthDate", is(LocalDate.of(2000,1,1).toString())));
//    }
//
//    @Test
//    @WithMockUser(authorities = "test_write")
//    void createNewPlayer_InvalidAttributes() throws Exception {
//        when(playerFacade.createNewPlayer(any(BasePlayerDTO.class))).thenReturn(playerOne);
//
//        for (BasePlayerDTO invalidDTO : invalidDTOs) {
//            mockMvc.perform(post("/api/players")
//                            .contentType(MediaType.APPLICATION_JSON)
//                            .content(objectMapper.writeValueAsString(invalidDTO)))
//                    .andExpect(status().isBadRequest());
//        }
//
//
//    }
//    @Test
//    @WithMockUser(authorities = "test_write")
//    void updatePlayer_Success() throws PlayerNotFoundException, Exception {
//        Long playerId = 1L;
//        BasePlayerDTO validPlayerDTO = new BasePlayerDTO(1L,
//                "John", "Updated Doe",
//                LocalDate.of(2000,1,1), 81, 76, 86,
//                91, 181, 76, 1100000);
//
//        PlayerDTO updatedPlayerDTO = new PlayerDTO(1L,1L,
//                "John", "Updated Doe",
//                LocalDate.of(2000,1,1), 81, 76, 86,
//                91, 181, 76, 1100000);
//
//        when(playerFacade.updatePlayer(eq(playerId), any(BasePlayerDTO.class))).thenReturn(updatedPlayerDTO);
//
//        mockMvc.perform(put("/api/player/{id}", playerId)
//                        .contentType(MediaType.APPLICATION_JSON)
//                        .content(objectMapper.writeValueAsString(validPlayerDTO)))
//                .andExpect(status().isOk())
//                .andExpect(jsonPath("$.lastName", is("Updated Doe")));
//    }
//
//    @Test
//    @WithMockUser(authorities = "test_write")
//    void updatePlayer_PlayerNotFound() throws Exception, PlayerNotFoundException {
//        Long playerId = 99L;
//        BasePlayerDTO playerDTO = new BasePlayerDTO(1L,
//                "John", "Doe", LocalDate.of(2000,1,1), 80,
//                75, 85, 90, 180,
//                75, 1000000);
//
//        when(playerFacade.updatePlayer(eq(playerId), any(BasePlayerDTO.class))).thenThrow(new PlayerNotFoundException( "Player with id: " + playerId + " was not found."));
//
//        mockMvc.perform(put("/api/player/{id}", playerId)
//                        .contentType(MediaType.APPLICATION_JSON)
//                        .content(objectMapper.writeValueAsString(playerDTO)))
//                .andExpect(status().isNotFound());
//    }
//
//    @Test
//    @WithMockUser(authorities = "test_write")
//    void updatePlayer_InvalidAttributes() throws Exception {
//        Long playerId = 1L;
//
//        for (BasePlayerDTO invalidDTO : invalidDTOs) {
//            mockMvc.perform(put("/api/player/{id}", playerId)
//                            .contentType(MediaType.APPLICATION_JSON)
//                            .content(objectMapper.writeValueAsString(invalidDTO)))
//                    .andExpect(status().isBadRequest());
//        }
//    }
//
//    @Test
//    @WithMockUser(authorities = "test_write")
//    void deletePlayer_Success() throws Exception, PlayerDeleteException, PlayerNotFoundException {
//        Long playerId = 1L;
//
//        when(playerFacade.deletePlayer(playerId)).thenReturn(playerOne);
//
//        mockMvc.perform(delete("/api/player/{id}", playerId))
//                .andExpect(status().isOk())
//                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
//                .andExpect(jsonPath("$.id", is(playerId.intValue())))
//                .andExpect(jsonPath("$.firstName", is("John")));
//
//        verify(playerFacade).deletePlayer(playerId);
//    }
//
//    @Test
//    @WithMockUser(authorities = "test_write")
//    void deletePlayer_NotFound() throws Exception, PlayerDeleteException, PlayerNotFoundException {
//        Long playerId = 99L;
//        when(playerFacade.deletePlayer(playerId)).thenThrow(new PlayerNotFoundException( "Player with id: " + playerId + " was not found."));
//
//        mockMvc.perform(delete("/api/player/{id}", playerId))
//                .andExpect(status().isNotFound());
//
//        verify(playerFacade).deletePlayer(playerId);
//    }
//    @Test
//    @WithMockUser(authorities = "test_write")
//    void deletePlayer_Failed_PlayerInTeam() throws Exception, PlayerDeleteException, PlayerNotFoundException {
//        Long playerId = 2L;
//        when(playerFacade.deletePlayer(playerId)).thenThrow(new PlayerDeleteException());
//
//        mockMvc.perform(delete("/api/player/{id}", playerId))
//                .andExpect(status().isConflict())
//                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
//                .andExpect(jsonPath("$.message", is("Cannot delete player that is in a team.")));
//
//        verify(playerFacade).deletePlayer(playerId);
//    }
//
//}
