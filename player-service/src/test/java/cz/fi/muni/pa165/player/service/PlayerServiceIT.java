//package cz.fi.muni.pa165.player.service;
//
//import com.fasterxml.jackson.databind.ObjectMapper;
//import cz.fi.muni.pa165.dtos.BasePlayerDTO;
//import org.junit.jupiter.api.Test;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.http.MediaType;
//import org.springframework.test.web.servlet.MockMvc;
//
//import java.time.LocalDate;
//
//import static org.hamcrest.Matchers.is;
//import static org.hamcrest.Matchers.nullValue;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
//
//@SpringBootTest
//@AutoConfigureMockMvc
//public class PlayerServiceIT {
//
//    @Autowired
//    private MockMvc mockMvc;
//
//    @Autowired
//    private ObjectMapper objectMapper;
//
//    @Test
//    void getAllPlayers() throws Exception {
//        mockMvc.perform(get("/api/players"))
//                .andExpect(status().isOk())
//                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
//                .andExpect(jsonPath("$").isArray());
//    }
//    @Test
//    void getPlayer_NotFound() throws Exception {
//        mockMvc.perform(get("/api/players/{id}", Long.MAX_VALUE))
//                .andExpect(status().isNotFound());
//    }
//
//    @Test
//    void player_LifeCycle_CreateUpdateDelete() throws Exception {
//        // Create a new player
//        BasePlayerDTO newPlayer = new BasePlayerDTO(null, "Jane", "Doe",
//                LocalDate.of(2000,1,1), 85, 80, 85, 90, 175, 70, 1500000);
//        String playerJson = objectMapper.writeValueAsString(newPlayer);
//
//        String playerLocation = mockMvc.perform(post("/api/players")
//                        .contentType(MediaType.APPLICATION_JSON)
//                        .content(playerJson))
//                .andExpect(status().isCreated())
//                .andExpect(jsonPath("$.firstName", is("Jane")))
//                .andExpect(jsonPath("$.lastName", is("Doe")))
//                .andReturn().getResponse().getHeader("Location");
//
//        // Extract player ID from the location URL
//        assert playerLocation != null;
//        String playerId = playerLocation.substring(playerLocation.lastIndexOf('/') + 1);
//
//        // Update the player, add team
//        newPlayer.setTeamId(1L);
//        mockMvc.perform(put("/api/player/{id}", playerId)
//                        .contentType(MediaType.APPLICATION_JSON)
//                        .content(objectMapper.writeValueAsString(newPlayer)))
//                .andExpect(status().isOk())
//                .andExpect(jsonPath("$.teamId", is(1)));
//
//        // Attempt to delete the player
//        // Expecting failure because the player is in a team
//        mockMvc.perform(delete("/api/player/{id}", playerId))
//                .andExpect(status().isConflict());
//
//        // Update the player to remove them from the team
//        newPlayer.setTeamId(null);
//        mockMvc.perform(put("/api/player/{id}", playerId)
//                        .contentType(MediaType.APPLICATION_JSON)
//                        .content(objectMapper.writeValueAsString(newPlayer)))
//                .andExpect(status().isOk())
//                .andExpect(jsonPath("$.teamId", is(nullValue())));
//
//        //  Successfully delete the player
//        mockMvc.perform(delete("/api/player/{id}", playerId))
//                .andExpect(status().isOk());
//
//        // test that its deleted
//        mockMvc.perform(get("/api/players/{id}", playerId))
//                .andExpect(status().isNotFound());
//    }
//
//}
