package cz.muni.pa165.oauth2.client;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient;
import org.springframework.security.oauth2.client.annotation.RegisteredOAuth2AuthorizedClient;
import org.springframework.security.oauth2.core.oidc.user.OidcUser;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ClientController {

    private static final Logger log = LoggerFactory.getLogger(ClientController.class);

    @GetMapping("/")
    public String index(final Model model, @AuthenticationPrincipal final OidcUser user) {
        log.debug("********************************************************");
        log.debug("* index() called                                       *");
        log.debug("********************************************************");
        log.debug("user {}", user == null ? "is anonymous" : user.getSubject());

        model.addAttribute("user", user);
        return "index";
    }

    @GetMapping("registered")
    public String registered(final Model model, @AuthenticationPrincipal final OidcUser user,
            @RegisteredOAuth2AuthorizedClient final OAuth2AuthorizedClient oauth2Client) {
        log.debug("********************************************************");
        log.debug("* registered() called                                  *");
        log.debug("********************************************************");

        model.addAttribute("user", user);
        model.addAttribute("oauth2Client", oauth2Client);
        return "index";
    }
}
