package cz.fi.muni.pa165.dtos;

import cz.fi.muni.pa165.enums.MatchStatus;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.PositiveOrZero;

import java.time.LocalDateTime;

@Schema(description = "Data transfer object for Match information.")
public class MatchDTO extends BaseMatchDTO {

    @Schema(description = "The ID of the match.", example = "1")
    @NotNull(message = "validation.notNull.MatchID")
    private Long id;

    public MatchDTO(Long id,
                    @NotNull(message = "validation.notNull.status") MatchStatus status,
                    @PositiveOrZero(message = "validation.positiveOrZero.team1Score") Integer team1Score,
                    @PositiveOrZero(message = "validation.positiveOrZero.team2Score") Integer team2Score,
                    @NotNull(message = "validation.notNull.matchTime") LocalDateTime matchTime) {
        super(status, team1Score, team2Score, matchTime);
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
