package cz.fi.muni.pa165.enums;

public enum MatchStatus {
    TEAM1_WIN,
    TEAM2_WIN,
    TIE,
    ONGOING,
    PLANNED,
}
