package cz.fi.muni.pa165.enums;

public enum UserRole {
    ADMIN, USER
}
