package cz.fi.muni.pa165.exceptions;

public class PlayerNotFoundException extends Throwable {
    public PlayerNotFoundException(String message) {
        super(message);
    }
}
