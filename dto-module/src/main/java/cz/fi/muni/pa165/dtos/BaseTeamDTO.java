package cz.fi.muni.pa165.dtos;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.PositiveOrZero;

@Schema(description = "Base Data Transfer Object for Hockey Team information without ID.")
public class BaseTeamDTO {

    @NotBlank(message = "validation.notBlank.teamName")
    @Schema(description = "The name of the team.", example = "NewYork Rangers")
    private String name;

    @NotNull(message = "validation.notNull.budget")
    @PositiveOrZero(message = "validation.positiveOrZero.budget")
    @Schema(description = "The budget of the team.", example = "60_000")
    private Integer budget;

    @NotNull(message = "validation.notNull.freeSpots")
    @PositiveOrZero(message = "validation.positiveOrZero.freeSpots")
    @Schema(description = "The free spots for new players", example = "6")
    private Integer freeSpots;

    @Schema(description = "Manager ID which owns the team", example = "6")
    private Long managerId;

    public BaseTeamDTO() { }

    public BaseTeamDTO(String name, Integer budget, Integer freeSpots, Long managerId) {
        this.name = name;
        this.budget = budget;
        this.freeSpots = freeSpots;
        this.managerId = managerId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getBudget() {
        return budget;
    }

    public void setBudget(Integer budget) {
        this.budget = budget;
    }

    public Integer getFreeSpots() {
        return freeSpots;
    }

    public void setFreeSpots(Integer freeSpots) {
        this.freeSpots = freeSpots;
    }

    public Long getManagerId() {
        return managerId;
    }

    public void setManagerId(Long managerId) {
        this.managerId = managerId;
    }
}