package cz.fi.muni.pa165.dtos;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;

import java.time.LocalDate;

@Schema(description = "Data transfer object for Hockey Player information.")
public class PlayerDTO extends BasePlayerDTO {

    @NotNull(message = "validation.notNull.PlayerID")
    @Schema(description = "The ID of the player.", example = "1")
    private Long id;

    public PlayerDTO(Long id,
                     Long teamId,
                     @NotBlank(message = "validation.notBlank.firstName") String firstName,
                     @NotBlank(message = "validation.notBlank.lastName") String lastName,
                        @NotNull(message = "validation.notNull.birthDate") LocalDate birthDate,
                     @NotNull(message = "validation.notNull.skill") @Positive(message = "validation.positive.skill") Integer skill,
                     @NotNull(message = "validation.notNull.strength") @Positive(message = "validation.positive.strength") Integer strength,
                     @NotNull(message = "validation.notNull.stamina") @Positive(message = "validation.positive.stamina") Integer stamina,
                     @NotNull(message = "validation.notNull.speed") @Positive(message = "validation.positive.speed") Integer speed,
                     @NotNull(message = "validation.notNull.height") @Positive(message = "validation.positive.height") Integer height,
                     @NotNull(message = "validation.notNull.weight") @Positive(message = "validation.positive.weight") Integer weight,
                     @NotNull(message = "validation.notNull.price") @Positive(message = "validation.positive.price") Integer price) {
        super(teamId, firstName, lastName, birthDate, skill, strength, stamina, speed, height, weight, price);
        this.id = id;
    }

    public PlayerDTO(Long teamId, @NotBlank(message = "validation.notBlank.firstName") String firstName, @NotBlank(message = "validation.notBlank.lastName") String lastName, @NotNull(message = "validation.notNull.birthDate") LocalDate birthDate, @NotNull(message = "validation.notNull.skill") @Positive(message = "validation.positive.skill") Integer skill, @NotNull(message = "validation.notNull.strength") @Positive(message = "validation.notNull.strength") Integer strength, @NotNull(message = "validation.notNull.stamina") @Positive(message = "validation.positive.stamina") Integer stamina, @NotNull(message = "validation.notNull.speed") @Positive(message = "validation.positive.speed") Integer speed, @NotNull(message = "validation.notNull.height") @Positive(message = "validation.positive.height") Integer height, @NotNull(message = "validation.notNull.weight") @Positive(message = "validation.positive.weight") Integer weight, @NotNull(message = "validation.notNull.price") @Positive(message = "validation.positive.price") Integer price, Long id) {
        super(teamId, firstName, lastName, birthDate, skill, strength, stamina, speed, height, weight, price);
        this.id = id;
    }
    public PlayerDTO() {
        super();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

}