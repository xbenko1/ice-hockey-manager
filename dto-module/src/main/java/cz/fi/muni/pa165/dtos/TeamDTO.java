package cz.fi.muni.pa165.dtos;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.PositiveOrZero;

@Schema(description = "Data transfer object for Hockey Team information.")
public class TeamDTO extends BaseTeamDTO {

    @NotNull(message = "validation.notNull.TeamID")
    @Schema(description = "The ID of the team.", example = "1")
    private Long id;

    public TeamDTO(Long id,
            @NotBlank(message = "validation.notBlank.teamName") String name,
            @NotNull(message = "validation.notNull.budget") @PositiveOrZero(message = "validation.positiveOrZero.budget") Integer budget,
            @NotNull(message = "validation.notNull.freeSpots") @PositiveOrZero(message = "validation.positiveOrZero.freeSpots") Integer freeSpots,
            Long managerId) {
        super(name, budget, freeSpots, managerId);

        this.id = id;
    }

    public TeamDTO() {
        super();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
