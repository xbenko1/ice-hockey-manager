package cz.fi.muni.pa165.dtos;

import cz.fi.muni.pa165.enums.UserRole;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

import java.util.List;

@Schema(name = "UserDto", description = "DTO for User")
public class UserDTO {

        @NotNull(message = "validation.notNull.UserID")
        @Schema(description = "ID of the user")
        private Long id;

        @NotBlank(message = "validation.notBlank.email")
        @Schema(description = "The email of the user.", example = "johndoemail.com")
        private String email;

        @Schema(description = "The roles of the user.", example = "[\"USER\", \"ADMIN\"]")
        private List<UserRole> roles;
        public Long getId() {
                return id;
        }

        public void setId(Long id) {
                this.id = id;
        }

        public String getEmail() {
                return email;
        }

        public void setEmail(String email) {
                this.email = email;
        }

        public List<UserRole> getRoles() {
                return roles;
        }

        public void setRoles(List<UserRole> roles) {
                this.roles = roles;
        }
}
