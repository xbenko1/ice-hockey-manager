package cz.fi.muni.pa165.dtos;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;


import java.time.LocalDate;

@Schema(description = "Base Data Transfer Object for Hockey Player information without ID.")
public class BasePlayerDTO {

    @Schema(description = "The team ID to which the player belongs.", example = "10")
    private Long teamId;

    @NotBlank(message = "validation.notBlank.firstName")
    @Schema(description = "The first name of the player.", example = "John")
    private String firstName;

    @NotBlank(message = "validation.notBlank.lastName")
    @Schema(description = "The last name of the player.", example = "Doe")
    private String lastName;

    @NotNull(message = "validation.notNull.birthDate")
    @Schema(description = "The birth date of the player.", example = "1996-01-01")
    private LocalDate birthDate;

    @NotNull(message = "validation.notNull.skill")
    @Positive(message = "validation.positive.skill")
    @Schema(description = "The skill level of the player.", example = "80")
    private Integer skill;

    @NotNull(message = "validation.notNull.strength")
    @Positive(message = "validation.positive.strength")
    @Schema(description = "The strength of the player.", example = "75")
    private Integer strength;

    @NotNull(message = "validation.notNull.stamina")
    @Positive(message = "validation.positive.stamina")
    @Schema(description = "The stamina of the player.", example = "85")
    private Integer stamina;

    @NotNull(message = "validation.notNull.speed")
    @Positive(message = "validation.positive.speed")
    @Schema(description = "The speed of the player.", example = "90")
    private Integer speed;

    @NotNull(message = "validation.notNull.height")
    @Positive(message = "validation.positive.height")
    @Schema(description = "The height of the player in centimeters.", example = "180")
    private Integer height;

    @NotNull(message = "validation.notNull.weight")
    @Positive(message = "validation.positive.weight")
    @Schema(description = "The weight of the player in kilograms.", example = "75")
    private Integer weight;

    @NotNull(message = "validation.notNull.price")
    @Positive(message = "validation.positive.price")
    @Schema(description = "The market price of the player.", example = "1000000")
    private Integer price;

    public BasePlayerDTO() {
    }
    public BasePlayerDTO(Long teamId, String firstName, String lastName, LocalDate birthDate, Integer skill, Integer strength, Integer stamina, Integer speed, Integer height, Integer weight, Integer price) {
        this.teamId = teamId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthDate = birthDate;
        this.skill = skill;
        this.strength = strength;
        this.stamina = stamina;
        this.speed = speed;
        this.height = height;
        this.weight = weight;
        this.price = price;
    }

    public Long getTeamId() {
        return teamId;
    }

    public void setTeamId(Long teamId) {
        this.teamId = teamId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public Integer getSkill() {
        return skill;
    }

    public void setSkill(Integer skill) {
        this.skill = skill;
    }

    public Integer getStrength() {
        return strength;
    }

    public void setStrength(Integer strength) {
        this.strength = strength;
    }

    public Integer getStamina() {
        return stamina;
    }

    public void setStamina(Integer stamina) {
        this.stamina = stamina;
    }

    public Integer getSpeed() {
        return speed;
    }

    public void setSpeed(Integer speed) {
        this.speed = speed;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }
}