package cz.fi.muni.pa165.dtos;

import cz.fi.muni.pa165.enums.MatchStatus;
import jakarta.validation.constraints.PositiveOrZero;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;

import java.time.LocalDateTime;

public class BaseMatchDTO {

    @Schema(description = "The status of the match.", example = "ONGOING")
    @NotNull(message = "validation.notNull.status")
    private MatchStatus status;

    @Schema(description = "The score of the first team.", example = "3")
    @PositiveOrZero(message = "validation.positiveOrZero.team1Score")
    private Integer team1Score;

    @Schema(description = "The score of the second team.", example = "2")
    @PositiveOrZero(message = "validation.positiveOrZero.team2Score")
    private Integer team2Score;

    @Schema(description = "The date and time of the match.", example = "2024-04-01T18:00:00")
    @NotNull(message = "validation.notNull.matchTime")
    private LocalDateTime matchTime;

    public BaseMatchDTO(MatchStatus status, Integer team1Score, Integer team2Score, LocalDateTime matchTime) {
        this.status = status;
        this.team1Score = team1Score;
        this.team2Score = team2Score;
        this.matchTime = matchTime;
    }

    public MatchStatus getStatus() {
        return status;
    }

    public void setStatus(MatchStatus status) {
        this.status = status;
    }

    public Integer getTeam1Score() {
        return team1Score;
    }

    public void setTeam1Score(Integer team1Score) {
        this.team1Score = team1Score;
    }

    public Integer getTeam2Score() {
        return team2Score;
    }

    public void setTeam2Score(Integer team2Score) {
        this.team2Score = team2Score;
    }

    public LocalDateTime getMatchTime() {
        return matchTime;
    }

    public void setMatchTime(LocalDateTime matchTime) {
        this.matchTime = matchTime;
    }
}

