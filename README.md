# Ice Hockey Manager
## Assigment definition
Several human players (at least two) can manage their hockey teams out of a list of real ice hockey teams of several championships across Europe. Human players can pick their team and add / remove ice hockey players from a list of available free agents. There is a schedule of games and results will be generated taking into account the players characteristics (no need to have some advanced algorithm simulating games: just a simple randomization will do it!). Admin can put new hockey players in the main list of free agents and change their attributes before they are selected by other human players. If you want, you can implement a budget system for each team, so that players can be bought and sold based on the financial availability of teams.


## Developers
| Name              | UCO                                            |
|-------------------|------------------------------------------------|
| Samuel Benko      | [514449](https://is.muni.cz/auth/osoba/514449) |
| Valentína Monková | [485066](https://is.muni.cz/auth/osoba/485066) |
| Jakub Košvanec    | [485430](https://is.muni.cz/auth/osoba/485430) |
| Maksim Syrvachev  | [553685](https://is.muni.cz/auth/osoba/553685)  |

## Running the application
### Running the application with docker
1. Clone the repository
2. Run `docker-compose up` in the root of the repository
3. For accessing the services, see the section below or the `docker-compose.yml` file for the ports
4. To stop the application, run `docker-compose down`

### Running the application without docker
1. Clone the repository
2. Run `mvn clean install` in the root of the repository
3. cd into each service and run `mvn spring-boot:run` in each service
4. `mvn -f match-service/pom.xml spring-boot:run`
5. `mvn -f player-service/pom.xml spring-boot:run`
6. `mvn -f team-service/pom.xml spring-boot:run`
7. `mvn -f user-service/pom.xml spring-boot:run`
8. `mvn -f oauth2_client/pom.xml spring-boot:run`

## Runnable Scenarios

There are two Python scripts in the `locust-testing` folder that simulate different user scenarios. These scripts require a bearer token, which can be obtained from the auth service at port 8080.

1. **locust-user.py**: This script simulates a user who manages a team. The user can get a free team, and add available players to the team. This script can be executed by anyone who has a valid bearer token.

2. **locust-admin.py**: This script simulates an admin who can create new teams and players. This script can only be run by the developers of this project, as we are the only ones with admin privileges in the database.
## Accessing the application
- Player Service
  - API Documentation: http://localhost:8081/openapi
  - Swagger UI: http://localhost:8081/swagger-ui.html
- Team Service
  - API Documentation: http://localhost:8082/openapi
  - Swagger UI: http://localhost:8082/swagger-ui.html
- Match Service
  - API Documentation: http://localhost:8083/openapi
  - Swagger UI: http://localhost:8083/swagger-ui.html
- User Service 
  - API Documentation: http://localhost:8084/openapi
  - Swagger UI: http://localhost:8084/swagger-ui.html
- Grafana
  - http://localhost:3000
  - Username: admin
  - Password: securepassword
- Prometheus
  - http://localhost:9090
- OAuth2 Authorization Service
  - http://localhost:8080

## Services
- User Service 
- Team Service 
- Match Service
- Player Service
- OAuth2 Authorization Service

### User service
- Registration of Team Manager
- Login + Session

ADMIN ONLY:
- Acceptation  of Team Manager Registration
- Login + Session

### Team Service
- View Teams
- Pick a team
- Add player to team
- Remove player from team

ADMIN ONLY:
- CRUD Team

### Match Service
- View Matches

ADMIN ONLY:
- Schedule Match

### Player Service
- View Free Agents

ADMIN ONLY:
- CRUD free agents


## Use case and DTO diagrams
![Use case diagram](/diagrams/Use%20Case%20Diagram.png)
![Dto Class diagram](/diagrams/DTO%20Class%20diagram.png)