import sys
import time
import random
import logging

from locust import HttpUser, task, between, SequentialTaskSet
from http import HTTPStatus

player_service = "http://localhost:8081/api"
team_service = "http://localhost:8082/api"
match_service = "http://localhost:8083/api"
user_service = "http://localhost:8084/api"

max_retries = 3

manager_id = 1

names = [
    "John",
    "Alexei",
    "Connor",
    "Samantha",
    "Nikolai",
    "Emily",
    "Liam",
    "Gabrielle",
    "Matej",
    "Elena"
]

surnames = [
    "Smith",
    "Petrov",
    "O'Reilly",
    "Johnson",
    "Ivanov",
    "Chen",
    "Murphy",
    "Dubois",
    "Novak",
    "Rodriguez"
]

team_names = [
    "Toronto Maple Leafs",
    "New York Rangers",
    "Chicago Blackhawks",
    "Montreal Canadiens",
    "Los Angeles Kings"
]

def get_random_player():
    return {
        "firstName": random.choice(names),
        "lastName": random.choice(surnames),
        "birthDate": "2024-05-10T10:20:30.400",
        "skill": random.randint(10, 100),
        "strength": random.randint(10, 100),
        "stamina": random.randint(10, 100),
        "speed": random.randint(10, 100),
        "height": random.randint(145, 190),
        "weight": random.randint(75, 100),
        "price": random.randint(1000, 150000)
    }


def make_new_team():
           return {
               "name": random.choice(team_names),
               "budget": random.randint(50_000, 1_750_000),
               "freeSpots": 21,
               "managerId": None
           }


class Admin(HttpUser):

    def on_start(self):
        # only our team as an admin seeded in db can run this script
        logging.basicConfig(level=logging.DEBUG, format='%(asctime)s - %(levelname)s - %(message)s')
        logging.info("Starting Admin Testing")
        token = sys.argv[1]
        self.client.headers = {'Authorization': "Bearer " + token}

    @task
    class AdminTesting(SequentialTaskSet):
        wait_time = between(1, 3)

        def post_request(self, url, json=None, status_code=HTTPStatus.OK):
            retry_count = 1
            while True:
                if retry_count > max_retries:
                    logging.error("Max retries exceeded for POST to %s with data %s", url, json)
                    raise Exception("Failed to post a response after {} retries".format(max_retries))
                response = self.client.post(url, json=json)
                if response.status_code == status_code:
                    logging.info("POST to %s successful: %s", url, response.text)
                    break
                else:
                    logging.warning("Failed POST to %s on attempt %d: Status %d, Response %s",
                                    url, retry_count, response.status_code, response.text)
                retry_count += 1
                time.sleep(2 * retry_count)
            return response

        def get_request(self, url, status_code=HTTPStatus.OK):
            retry_count = 1
            while True:
                if retry_count > max_retries:
                    logging.error("Max retries exceeded for GET from %s", url)
                    raise Exception("Failed to get a response after {} retries".format(max_retries))
                response = self.client.get(url)
                if response.status_code == status_code:
                    logging.info("GET from %s successful: %s", url, response.text)
                    break
                else:
                    logging.warning("Failed GET from %s on attempt %d: Status %d, Response %s",
                                    url, retry_count, response.status_code, response.text)
                retry_count += 1
                time.sleep(2 * retry_count)
            return response

        @task
        def create_new_team(self):
            logging.info("Creating new team")
            new_team = make_new_team()
            created_team = self.post_request(team_service + "/teams", new_team, HTTPStatus.CREATED)
            logging.debug("Created team response: %s", created_team.text)

        @task
        def create_new_players(self):
            logging.info("Creating new players")
            for _ in range(5):
                player = get_random_player()
                created_player = self.post_request(player_service + "/players", player, HTTPStatus.CREATED)
                logging.debug("Created player response: %s", created_player.text)