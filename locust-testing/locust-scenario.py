import sys
import time
import random
import logging
from datetime import datetime

from locust import HttpUser, task, between, SequentialTaskSet
from http import HTTPStatus

player_service = "http://localhost:8081/api"
team_service = "http://localhost:8082/api"
match_service = "http://localhost:8083/api"
user_service = "http://localhost:8084/api"

max_retries = 3

manager_id = 1

names = [
    "John",
    "Alexei",
    "Connor",
    "Samantha",
    "Nikolai",
    "Emily",
    "Liam",
    "Gabrielle",
    "Matej",
    "Elena"
]

surnames = [
    "Smith",
    "Petrov",
    "O'Reilly",
    "Johnson",
    "Ivanov",
    "Chen",
    "Murphy",
    "Dubois",
    "Novak",
    "Rodriguez"
]

team_names = [
    "Toronto Maple Leafs",
    "New York Rangers",
    "Chicago Blackhawks",
    "Montreal Canadiens",
    "Los Angeles Kings"
]

def get_random_player():
    return {
        "firstName": random.choice(names),
        "lastName": random.choice(surnames),
        "birthDate": "2024-05-10T10:20:30.400",
        "skill": random.randint(10, 100),
        "strength": random.randint(10, 100),
        "stamina": random.randint(10, 100),
        "speed": random.randint(10, 100),
        "height": random.randint(145, 190),
        "weight": random.randint(75, 100),
        "price": random.randint(1000, 150000)
    }


def make_new_team():
    return {
        "name": random.choice(team_names),
        "budget": random.randint(50_000, 1_750_000),
        "freeSpots": 21,
        "managerId": None
    }

def make_new_match(team_one_id, team_two_id):
    now = datetime.now()
    local_date_time = now.strftime('%Y-%m-%dT%H:%M:%S')
    return {
        "status": "ONGOING",
        "team1Score": team_one_id,
        "team2Score": team_two_id,
        "matchTime": local_date_time
    }

class Scenario(HttpUser):

    def on_start(self):
        token = sys.argv[1]
        self.client.headers = {'Authorization': "Bearer " + token}

    @task
    class NewTeamIntoMatchTesting(SequentialTaskSet):
        wait_time = between(1, 3)

        created_team_id = None
        free_agents_ids = []
        created_match_id = None

        def post_request(self, url, json=None, status_code=HTTPStatus.OK):
            retry_count = 1
            while True:
                if retry_count > max_retries:
                    logging.error("Max retries exceeded for POST to %s with data %s", url, json)
                    raise Exception("Failed to post a response after {} retries".format(max_retries))
                response = self.client.post(url, json=json)
                if response.status_code == status_code:
                    logging.info("POST to %s successful: %s", url, response.text)
                    break
                else:
                    logging.warning("Failed POST to %s on attempt %d: Status %d, Response %s",
                                    url, retry_count, response.status_code, response.text)
                retry_count += 1
                time.sleep(2 * retry_count)
            return response

        def get_request(self, url, status_code=HTTPStatus.OK):
            retry_count = 1
            while True:
                if retry_count > max_retries:
                    logging.error("Max retries exceeded for GET from %s", url)
                    raise Exception("Failed to get a response after {} retries".format(max_retries))
                response = self.client.get(url)
                if response.status_code == status_code:
                    logging.info("GET from %s successful: %s", url, response.text)
                    break
                else:
                    logging.warning("Failed GET from %s on attempt %d: Status %d, Response %s",
                                    url, retry_count, response.status_code, response.text)
                retry_count += 1
                time.sleep(2 * retry_count)
            return response
        
        @task
        def create_new_team(self):
            logging.info("Creating new team")
            new_team = make_new_team()
            created_team = self.post_request(team_service + "/teams", new_team, HTTPStatus.CREATED)
            logging.debug("Created team response: %s", created_team.text)
            self.created_team_id = created_team["id"]

        @task
        def create_new_players(self):
            logging.info("Creating new players")
            for _ in range(21):
                new_player = get_random_player()
                created_player = self.post_request(player_service + "/players", new_player, HTTPStatus.CREATED)
                logging.debug("Created player response: %s", created_player.text)

        @task
        def get_free_agents(self):
            logging.info("Getting free agents")
            free_agents = self.get_request(player_service + "/players/free-agents")
            for agent in free_agents:
                self.free_agents_ids.append(agent["id"])
                if(self.free_agents_ids.count() >= 21):
                    logging.debug("Free agents response: %s", ' '.join(self.free_agents_ids))
                    return
                
        @task
        def assign_free_agenst(self):
            logging.info("Assigning free agents")
            for agent_id in self.free_agents_ids:
                assigned_agent = self.post_request(player_service + "/players/" + agent_id + "/team/" + self.created_team_id)
                logging.debug("Assigned agent response: %s", assigned_agent.text)

        @task
        def create_new_match(self):
            logging.info("Creating new match")
            all_teams = self.get_request(team_names + "/teams")
            new_match = make_new_match(all_teams[0]["id"] ,self.created_team_id)
            new_created_match = self.post_request(match_service + "/matches", new_match , HTTPStatus.CREATED)
            logging.debug("Created match response: %s", new_created_match.text)
            self.created_match_id = new_created_match["id"]
        
        @task
        def set_team_one_as_winner(self):
            logging.info("Updating match")
            ongoing_match = self.get_request(match_service + "/match/" + self.created_match_id)
            logging.debug("Ongoing match response: %s", ongoing_match.text)
            ongoing_match["status"] = "TEAM2_WIN"
            updated_match = self.post_request(match_service + "/match/" + self.created_match_id, updated_match)
            logging.debug("Updated match response: %s", updated_match.text)