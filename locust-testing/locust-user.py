import sys
import time

from locust import HttpUser, task, between, SequentialTaskSet
from http import HTTPStatus

player_service = "http://localhost:8081/api"
team_service = "http://localhost:8082/api"
match_service = "http://localhost:8083/api"
user_service = "http://localhost:8084/api"

max_retries = 3

manager_id = 1


class User(HttpUser):

    def on_start(self):
        token = sys.argv[1]
        self.client.headers = {'Authorization': "Bearer " + token}

    @task
    class ManagerTesting(SequentialTaskSet):
        wait_time = between(1, 3)

        assigned_team_id = None

        def post_request(self, url, json=None):
            retry_count = 1
            while True:
                if retry_count > max_retries:
                    raise Exception("Failed to post a response")
                response = self.client.post(url, json=json)
                if response.status_code == HTTPStatus.OK:
                    break
                retry_count += 1
                time.sleep(2 * retry_count)
            return response

        def get_request(self, url):
            retry_count = 1
            while True:
                if retry_count > max_retries:
                    raise Exception("Failed to get a response")
                response = self.client.get(url)
                print(response.status_code)
                if response.status_code == HTTPStatus.OK:
                    break
                retry_count += 1
                time.sleep(2 * retry_count)
            return response

        @task
        def get_free_team(self):
            free_teams = self.get_request(team_service + "/teams/manager-less-teams")
            print(free_teams)
            free_team_id = free_teams[0]["id"]
            updated_team = self.post_request(team_service + "/teams/" + free_team_id + "/manger/" + manager_id)
            self.assigned_team_id = updated_team["id"]

        @task
        def add_available_players(self):
            players_without_team = self.get_request(player_service + "/players/free-agents")

            player_one_id = players_without_team[0]["id"]
            player_two_id = players_without_team[1]["id"]

            self.post_request(player_service + "/players/" + player_one_id + "/team/" + self.assigned_team_id)
            self.post_request(player_service + "/players/" + player_two_id + "/team/" + self.assigned_team_id)